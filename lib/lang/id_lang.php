<?php
$choose_lang = 'Pilih Bahasa';
$sidebar_menu1 = 'Home';
$sidebar_menu2 = 'Tentang Kami';
$sidebar_menu2_sub1 = 'Cerita Prosperita';
$sidebar_menu2_sub2 = 'Visi';
$sidebar_menu2_sub3 = 'Misi';
$sidebar_menu2_sub4 = 'Budaya';
$sidebar_menu3 = 'Solusi';
$sidebar_menu3_sub1 = 'ESET';
$sidebar_menu3_sub2 = 'GreyCortex';
$sidebar_menu3_sub3 = 'Xopero';
$sidebar_menu3_sub4 = 'Safetica';
$sidebar_menu3_sub5 = 'Flexera';
$sidebar_menu3_sub6 = 'Awanpintar.id';
$sidebar_menu4 = 'Partner';
$sidebar_menu5 = 'Registrasi Parter/Reseller';
$sidebar_menu6 = 'Partner Portal';
$sidebar_menu7 = 'Toko Online';
$sidebar_menu8 = 'Event';
$sidebar_menu9 = 'Hubungi Kami';
$sidebar_menu10 = 'Kebijakan Privasi';
?>