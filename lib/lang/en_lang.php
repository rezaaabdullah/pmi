<?php
$choose_lang = 'Choose Language';
$sidebar_menu1 = 'Home';
$sidebar_menu2 = 'About Us';
$sidebar_menu2_sub1 = 'Story of Prosperita';
$sidebar_menu2_sub2 = 'Vision';
$sidebar_menu2_sub3 = 'Mission';
$sidebar_menu2_sub4 = 'Culture';
$sidebar_menu3 = 'Our Solutions';
$sidebar_menu3_sub1 = 'ESET';
$sidebar_menu3_sub2 = 'GreyCortex';
$sidebar_menu3_sub3 = 'Xopero';
$sidebar_menu3_sub4 = 'Safetica';
$sidebar_menu3_sub5 = 'Flexera';
$sidebar_menu3_sub6 = 'Awanpintar.id';
$sidebar_menu4 = 'Our Partners';
$sidebar_menu5 = 'Partner Registration';
$sidebar_menu6 = 'Partner Portal';
$sidebar_menu7 = 'Our e-Store';
$sidebar_menu8 = 'Our Events';
$sidebar_menu9 = 'Contact Us';
$sidebar_menu10 = 'Privacy Policy';
?>