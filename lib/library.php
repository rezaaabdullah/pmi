<?php

$user_agent = $_SERVER['HTTP_USER_AGENT'];

function getOS() {

    global $user_agent;

    $os_platform  = "Unknown OS Platform";

    $os_array     = array(
                          '/windows nt 10/i'      =>  'Windows 10',
                          '/windows nt 6.3/i'     =>  'Windows 8.1',
                          '/windows nt 6.2/i'     =>  'Windows 8',
                          '/windows nt 6.1/i'     =>  'Windows 7',
                          '/windows nt 6.0/i'     =>  'Windows Vista',
                          '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                          '/windows nt 5.1/i'     =>  'Windows XP',
                          '/windows xp/i'         =>  'Windows XP',
                          '/windows nt 5.0/i'     =>  'Windows 2000',
                          '/windows me/i'         =>  'Windows ME',
                          '/win98/i'              =>  'Windows 98',
                          '/win95/i'              =>  'Windows 95',
                          '/win16/i'              =>  'Windows 3.11',
                          '/macintosh|mac os x/i' =>  'Mac OS X',
                          '/mac_powerpc/i'        =>  'Mac OS 9',
                          '/linux/i'              =>  'Linux',
                          '/ubuntu/i'             =>  'Ubuntu',
                          '/iphone/i'             =>  'iPhone',
                          '/ipod/i'               =>  'iPod',
                          '/ipad/i'               =>  'iPad',
                          '/android/i'            =>  'Android',
                          '/blackberry/i'         =>  'BlackBerry',
                          '/webos/i'              =>  'Mobile'
                    );

    foreach ($os_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $os_platform = $value;

    return $os_platform;
}

function getBrowser() {

    global $user_agent;

    $browser        = "Unknown Browser";

    $browser_array = array(
                            '/mobile/i'    => 'Handheld Browser',
                            '/msie/i'      => 'Internet Explorer',
                            '/firefox/i'   => 'Firefox',
                            '/safari/i'    => 'Safari',
                            '/chrome/i'    => 'Chrome',
                            '/edge/i'      => 'Edge',
                            '/opera|OPR/i'     => 'Opera',
                            '/netscape/i'  => 'Netscape',
                            '/maxthon/i'   => 'Maxthon',
                            '/konqueror/i' => 'Konqueror',
                            '/ucbrowser/i' => 'UC Browser'
                     );

    foreach ($browser_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $browser = $value;

    return $browser;
}

$logip        = $_SERVER['REMOTE_ADDR'];
$loghost      = $_SERVER['HTTP_HOST'];
$logserver    = $_SERVER['SERVER_SOFTWARE'];
$logos        = getOS();
$logbrowser   = getBrowser();

function templatefile($current_url)
{
	global $dir_tpl;

  $fn = substr(strrchr($current_url, '/'), 1);
  $fn_dir = substr($current_url, strpos($current_url, 'https/') + 6);
  $fn_dir = (strpos($fn_dir, '/')) ? substr($fn_dir, 0, strpos($fn_dir, '/')) : "";
  $fn = $dir_tpl. $fn_dir .'/'. strstr($fn, '.', true);

  if (!file_exists($fn .'.tpl'))
  {
    return $fn. 'index.tpl';
  }

  return $fn. '.tpl';
}

function url_jump($url, $alert = null)
{
?>
  <script language="javascript">
    <?php
    if ($alert != null)
    {
    ?>
    alert('<?php echo $alert ?>');
    <?php
    }

    if (preg_match('/^(?:.)*\.php$/', $url))
    {
    ?>
		window.location="<?php echo $url; ?>";
    <?php
    }
    else
    {
    ?>
    window.location="index.php";
    <?php
    }
    ?>
	</script>
<?php
	exit();
}
?>

<script type="text/javascript">
function validateEmail(email)
{
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (re.test(email))
  {
    return true;
  }
  else
  {
    return false;
  }
}
</script>
