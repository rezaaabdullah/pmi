<?php
include_once 'header.php';

$bg_img_name = '4.jpg';

$en_title = '';
$en_content = '';

$en_content_online_setion ='<h3>Online Reseller Registration</h3>
	<p>
	Online Reseller is for those who need promptness in obtaining the license for Customers/End-users.  The license in form of a softcopy could be obtained directly after you finish your order.
	</p>

	<p>
	You will get an account for the access to Prosperita Portal. With deposit system, you may perform any purchase within 24/7. The details of online reseller mechanism will be informed after you register. Currently, Online Reseller is available for the product of ESET Home Edition/Personal Solution and ESET Business Edition/Business Solution. Please choose according to your own capacity and register via the link below.
	</p>
	';

$en_btn_online_reg = 'Online Reseller Registration';
$en_form_online_note = 'This is the ONLINE Reseller Registration page. Please fill in the data in this column correctly and make sure the email address is valid so we can help you properly. Registration will not be processed if the data is incomplete, incorrect, or not in accordance with the allotment of cooperation.';
$en_type_online = 'With this I am interested in becoming an ONLINE Reseller of the product/products below (choose one)';

$en_content_offline_setion ='<h3>Offline Reseller Registration</h3>

	<p>Offline Reseller is designed for you who need discussion to get the right solution before making a purchase. A minimal purchase and license in form of soft case are valid for Personal Solutions and Media Kit for Business Solution. The purchase is done by sending the Purchase Order. Please choose the registration according to your project via the link below. </p>
	';

$en_btn_offline_reg = 'Offline Reseller Registration';
$en_form_offline_note = 'This is the OFFLINE Reseller Registration page. Please fill in the data in this column correctly and make sure the email address is valid so we can help you properly. Registration will not be processed if the data is incomplete, incorrect, or not in accordance with the allotment of cooperation.';
$en_type_offline = 'With this I am interested in becoming an OFFLINE Reseller of the product/products below (choose one)';

$en_shopname = 'Online Store Name';
$en_compname = 'Company Name';
$en_businesstype = 'Business Type';
$en_address = 'Address';
$en_country = 'Country';
$en_province= 'Province';
$en_city    = 'City';
$en_zip		= 'ZIP Code';
$en_website		= 'Website';
$en_contactperson	= 'Contact Person';
$en_email	= 'Email Address';
$en_phone	= 'Phone Number';
$en_mobile	= 'Mobile Phone Number';
$en_mobile_note = '*This number and device will be used as a token';
$en_taxno = 'Tax Number';
$en_taxcompname= 'Company Name';
$en_taxaddress = 'Address';
$en_taxprovince= 'Province';
$en_taxcity		= 'City';
$en_taxzip		= 'ZIP Code';
$en_tax_card	= 'Upload NPWP';
$en_comp_size	= 'Company Size';
$en_nib	= 'Upload NIB';
$en_siup	= 'Upload SIUP';
$en_domicile	= 'Upload Company Domicile';
$en_note		= 'Note';
$en_captcha		= 'Security Code';
$en_send		= 'SEND';
$en_seccode_msg = 'Invalid captcha';
$en_success_msg = 'Data Inserted';


$id_title = 'Registrasi Reseller';
$id_content = '<p>Kekuatan Prosperita adalah sinergi antara teknologi solusi keamanan dan strategi kemitraan yang disesuaikan dengan pasar Anda. Menyadari pentingnya jaringan mitra, kami telah merancang Program Mitra Prosperita untuk menjangkau kebutuhan mitra bisnis kami. Untuk itu kami mengundang Anda untuk berpartisipasi dalam mendistribusikan baik secara Online maupun Offline.</p>

	<p><b>Keuntungan menjadi reseller Prosperita:</b></p>
	<ol>
		<li>Produk sudah dikenal pasar</li>
		<li>Teknologi dapat diandalkan</li>
		<li>Keuntungan yang menarik</li>
		<li>Dukungan pelatihan dan tenaga ahli</li>
		<li>Dukungan materi sales dan marketing</li>
	</ol>';

$id_content_online_setion ='<p><b>Bila Anda memiliki kemampuan bisnis secara online:</b></p>

	<ul>
		<li>memiliki akses internet</li>
		<li>memiliki web site untuk melakukan display/berjualan</li>
		<li>memiliki kemampuan untuk memasarkan secara Online</li>
		<li>contoh: personal, e-store, warnet, business corner</li>
	</ul>

	<p>Sistem kami selalu siap selama 24/7 (24 jam sehari, 7 hari seminggu) untuk menyediakan lisensi bagi customer Anda, sehingga usaha Anda dapat berjalan terus sepanjang hari.</p>
	';
$id_btn_online_reg = 'Registrasi Reseller Online';
$id_form_online_note = 'Ini adalah halaman untuk Registrasi Reseller ONLINE. Harap isi data pada kolom ini dengan benar dan pastikan alamat email valid agar kami dapat membantu Anda dengan baik. Registrasi tidak akan diproses jika data tidak lengkap, tidak benar, atau tidak sesuai dengan peruntukan kerjasama.';
$id_type_online = 'Dengan ini saya tertarik untuk menjadi Reseller ONLINE untuk produk (pilih salah satu)';

$id_content_offline_setion ='<p><b>Bila Anda memiliki kemampuan bisnis secara offline:</b></p>

	<ul>
		<li>memiliki akses ke customer</li>
		<li>memiliki kemampuan memberikan solusi</li>
		<li>memiliki kemampuan untuk memasarkan</li>
		<li>memiliki lokasi berjualan yang menetap</li>
		<li>contoh: System Integrator, konsultan IT, toko komputer, toko software</li>
	</ul>

	<p>Tim kami selalu siap selama hari dan jam kerja, Senin-Jumat, 09.00-18.00wib, untuk menyediakan lisensi bagi customer Anda, sehingga usaha Anda dapat berjalan terus sepanjang hari.</p>
	';
$id_btn_offline_reg = 'Registrasi Reseller Offline';
$id_form_offline_note = 'Ini adalah halaman untuk Registrasi Reseller OFFLINE. Harap isi data pada kolom ini dengan benar dan pastikan alamat email valid agar kami dapat membantu Anda dengan baik. Registrasi tidak akan diproses jika data tidak lengkap, tidak benar, atau tidak sesuai dengan peruntukan kerjasama.';
$id_type_offline = 'Dengan ini saya tertarik untuk menjadi Reseller OFFLINE untuk produk (pilih salah satu)';

$id_shopname = 'Nama Toko Online';
$id_compname = 'Nama Perusahaan';
$id_businesstype = 'Tipe Usaha';
$id_address = 'Alamat';
$id_country = 'Negara';
$id_province= 'Provinsi';
$id_city    = 'Kota';
$id_zip		= 'Kode Pos';
$id_website		= 'Website';
$id_contactperson	= 'Penanggung Jawab';
$id_email	= 'Alamat Email';
$id_phone	= 'Nomor Telepon';
$id_mobile	= 'Nomor Handphone';
$id_mobile_note = '*nomor dan perangkat ini akan digunakan sebagai token';
$id_taxno = 'Nomor';
$id_taxcompname= 'Nama Perusahaan Terdaftar';
$id_taxaddress = 'Alamat';
$id_taxprovince= 'Provinsi';
$id_taxcity		= 'Kota';
$id_taxzip		= 'Kode Pos';
$id_tax_card	= 'Upload NPWP';
$id_comp_size	= 'Ukuran Perusahaan';
$id_nib	= 'Upload NIB';
$id_siup	= 'Upload SIUP';
$id_domicile	= 'Upload Surat Keterangan Domisili Perusahaan';
$id_note		= 'Catatan';
$id_captcha		= 'Kode Keamanan';
$id_send		= 'KIRIM';
$id_seccode_msg = 'Kode keamanan tidak valid';
$id_success_msg = 'Data berhasil tersimpan';

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
	if (isset($_POST['get_province_city']))
	{
		$headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($_POST['country'].$_POST['country']));
		$jsonparam = json_encode(array('city' => $_POST['country'], 'province' => $_POST['country']));
		$request = myCURL($url_apiprosperita_get_iso, $headers, $jsonparam);
		$response = json_decode($request, true);
	}

	if (isset($_POST['get_province_city_tax']))
	{
		$headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($_POST['country'].$_POST['country']));
		$jsonparam = json_encode(array('city' => $_POST['country'], 'province' => $_POST['country']));
		$request = myCURL($url_apiprosperita_get_iso, $headers, $jsonparam);
		$response = json_decode($request, true);
	}
}
else
{
	if (isset($_POST['submit']))
	{		
		if (isset($_SESSION['tokoeset_seccode']) && isset($_POST['seccode']) && $_SESSION['tokoeset_seccode'] == md5($_POST['seccode']))
		{
			$type = '';
			foreach ($_POST['type'] as $key => $value)
			{
				$type .= $value .', ';
			}

			$type = substr($type, 0, -2);

			if (isset($_GET['go']) && $_GET['go'] == 'online-reseller')
			{
				$url_api = $url_apiprosperita_reg_resonline;
			}
			elseif (isset($_GET['go']) && $_GET['go'] == 'offline-reseller')
			{
				$url_api = $url_apiprosperita_reg_resoffline;
			}

			$can_submit_form = true;

			if($_POST['comp_size'] === 'small')
			{
				if ($_FILES["tax_card"]["tmp_name"] != '' && $_FILES["nib"]["tmp_name"] != '')
				{
					$taxcard = base64_encode(file_get_contents($_FILES["tax_card"]["tmp_name"]));
					$taxcard_ext = strtolower(pathinfo($_FILES["tax_card"]["name"],PATHINFO_EXTENSION));
					$taxcard_size = $_FILES["tax_card"]["size"];

					$nib = base64_encode(file_get_contents($_FILES["nib"]["tmp_name"]));
					$nib_ext = strtolower(pathinfo($_FILES["nib"]["name"],PATHINFO_EXTENSION));
					$nib_size = $_FILES["nib"]["size"];

					$siup = '';
					$siup_ext = '';
	
					$domicile = '';
					$domicile_ext = '';

					if(($taxcard_ext == "jpg" || $taxcard_ext == "png" || $taxcard_ext == "jpeg" || $taxcard_ext == "pdf") &&
						($nib_ext == "jpg" || $nib_ext == "png" || $nib_ext == "jpeg" || $nib_ext == "pdf"))
					{					
						if ($taxcard_size <= 500000 && $nib_size <= 500000)
						{
							$_POST['shopname'] = isset($_POST['shopname']) ? $_POST['shopname'] : "";						
							if($_GET["go"] == "online-reseller" && $_POST["type"][0] == "1")
							{
								$can_submit_form = 'invalid type';
							}
						}
						else
						{
							$can_submit_form = 'file_validation_1';
						}
					}
					else
					{
						$can_submit_form = 'file_validation_2';
					}
				}
				else
				{
					$can_submit_form = 'Please complete all requested files.';
				}
			}
			else if($_POST['comp_size'] === 'medium-big')
			{
				if ($_FILES["tax_card"]["tmp_name"] != '' && $_FILES["nib"]["tmp_name"] != '' && 
					$_FILES["siup"]["tmp_name"] != '' && $_FILES["domicile"]["tmp_name"] != '')
				{
					$taxcard = base64_encode(file_get_contents($_FILES["tax_card"]["tmp_name"]));
					$taxcard_ext = strtolower(pathinfo($_FILES["tax_card"]["name"],PATHINFO_EXTENSION));
					$taxcard_size = $_FILES["tax_card"]["size"];
	
					$nib = base64_encode(file_get_contents($_FILES["nib"]["tmp_name"]));
					$nib_ext = strtolower(pathinfo($_FILES["nib"]["name"],PATHINFO_EXTENSION));
					$nib_size = $_FILES["nib"]["size"];
	
					$siup = base64_encode(file_get_contents($_FILES["siup"]["tmp_name"]));
					$siup_ext = strtolower(pathinfo($_FILES["siup"]["name"],PATHINFO_EXTENSION));
					$siup_size = $_FILES["siup"]["size"];
	
					$domicile = base64_encode(file_get_contents($_FILES["domicile"]["tmp_name"]));
					$domicile_ext = strtolower(pathinfo($_FILES["domicile"]["name"],PATHINFO_EXTENSION));
					$domicile_size = $_FILES["domicile"]["size"];
	
					if(($taxcard_ext == "jpg" || $taxcard_ext == "png" || $taxcard_ext == "jpeg" || $taxcard_ext == "pdf") &&
						 ($nib_ext == "jpg" || $nib_ext == "png" || $nib_ext == "jpeg" || $nib_ext == "pdf") &&
						 ($siup_ext == "jpg" || $siup_ext == "png" || $siup_ext == "jpeg" || $siup_ext == "pdf") && 
						 ($domicile_ext == "jpg" || $domicile_ext == "png" || $domicile_ext == "jpeg" || $domicile_ext == "pdf"))
					{					
						if ($taxcard_size <= 500000 && $nib_size <= 500000 && $siup_size <= 500000 && $domicile_size <= 500000)
						{
							$_POST['shopname'] = isset($_POST['shopname']) ? $_POST['shopname'] : "";						
							if($_GET["go"] == "online-reseller" && $_POST["type"][0] == "1")
							{
								$can_submit_form = 'invalid type';
							}
						}
						else
						{
							$can_submit_form = 'file_validation_1';
						}
					}
					else
					{
						$can_submit_form = 'file_validation_2';
					}
				}
				else
				{
					$can_submit_form = 'Please complete all requested files.';
				}
			}

			if($can_submit_form === true)
			{
				$headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($type.$_POST['compname'].$_POST['email']));
				$jsonparam = json_encode(array('type' => $type, 'shopname' => $_POST['shopname'],
				'compname' => $_POST['compname'], 'businesstype' => $_POST['businesstype'],
				'address' => $_POST['address'], 'city' => $_POST['city'],
				'province' => $_POST['province'], 'zip' => $_POST['zip'], 'website' => $_POST['website'],
				'contactperson' => $_POST['contactperson'], 'email' => $_POST['email'],
				'phone' => $_POST['phone'], 'mobile' => $_POST['mobile'], 'taxno' => $_POST['taxno'],
				'taxcompname' => $_POST['taxcompname'], 'taxaddress' => $_POST['taxaddress'],
				'taxprovince' => $_POST['taxprovince'], 'taxzip' => $_POST['taxzip'],
				'taxfilecontent' => $taxcard, 'taxfileext' => $taxcard_ext, 
				'nibfilecontent' => $nib, 'nibfileext' => $nib_ext,
				'siupfilecontent' => $siup, 'siupfileext' => $siup_ext, 
				'domisilifilecontent' => $domicile, 'domisilifileext' => $domicile_ext, 
				'note' => $_POST['note'],
				'logip' => $logip, 'loghost' => $loghost, 'logserver' => $logserver, 'logos' => $logos,
				'logbrowser' => $logbrowser));
				$request = myCURL($url_api, $headers, $jsonparam);
				$response = json_decode($request, true);
			}
			else
			{
				$response['errlog'] = $can_submit_form;
			}
		}
		else
		{
			$seccode_status = false;
		}
	}
}

include_once 'footer.php';
?>
