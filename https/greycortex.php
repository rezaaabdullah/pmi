<?php
	include_once 'header.php';
	
	$bg_img_name = '5.jpg';//'8.jpg';
	$product_icon = 'greycortex.png';

	$en_title = 'GREYCORTEX';
	$en_content = '<p>Intelligent network traffic analysis. GreyCortex with Mendel technology integrates the analysis of network traffic by using artificial intelligence (machine learning) to monitor and detect the network security gap by using proactive detection against the attack on network infrastructure of the company.</p>';

	$id_title = 'GREYCORTEX';
	$id_content = '<p>Intelligent network traffic analysis. GreyCortex dengan teknologi Mendel menggabungkan analisis lalu lintas jaringan dengan kecerdasan buatan (machine learning) untuk memantau dan mendeteksi celah keamanan jaringan dengan deteksi proaktif terhadap serangan pada infrastruktur jaringan perusahaan.</p>';


	include_once 'footer.php';
?>	