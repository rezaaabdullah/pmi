<nav class="sidebar">
    <ul class="sidebar-menu">
        <li>
            <?php
            if ($active_page == 'index.php' || $active_page == 'https') 
            {
            ?>
            <a href="index.php" class="active">
            <?php
            }
            else
            {
            ?>
            <a href="index.php">
            <?php
            }
            ?>
                <?php echo $sidebar_menu1; ?>
            </a>
        </li>

        <li>
            <?php
            if ($active_page == 'story.php' || $active_page == 'vision.php' || $active_page == 'mission.php' || $active_page == 'culture.php')
            {
            ?>
            <a href="#" class="submenu-toggle active">
            <?php
            }
            else
            {
            ?>
            <a href="#" class="submenu-toggle">
            <?php
            }
            ?>
                <?php echo $sidebar_menu2; ?>
            </a>

            <ul class="submenu-wrapper">
                <li>
                    <?php
                    if ($active_page == 'story.php') 
                    {
                    ?>
                    <a href="story.php" class="active">
                        <?php echo $sidebar_menu2_sub1; ?>
                    </a>
                    <?php
                    }
                    else
                    {
                    ?>
                    <a href="story.php">
                        <?php echo $sidebar_menu2_sub1; ?>
                    </a>
                    <?php
                    }
                    ?>
                </li>

                <li>
                    <?php
                    if ($active_page == 'vision.php') 
                    {
                    ?>
                    <a href="vision.php" class="active">
                        <?php echo $sidebar_menu2_sub2; ?>
                    </a>
                    <?php
                    }
                    else
                    {
                    ?>
                    <a href="vision.php">
                        <?php echo $sidebar_menu2_sub2; ?>
                    </a>
                    <?php
                    }
                    ?>
                </li>

                <li>
                    <?php
                    if ($active_page == 'mission.php') 
                    {
                    ?>
                    <a href="mission.php" class="active">
                        <?php echo $sidebar_menu2_sub3; ?>
                    </a>
                    <?php
                    }
                    else
                    {
                    ?>
                    <a href="mission.php">
                        <?php echo $sidebar_menu2_sub3; ?>
                    </a>
                    <?php
                    }
                    ?>
                </li>

                <li>
                    <?php
                    if ($active_page == 'culture.php') 
                    {
                    ?>
                    <a href="culture.php" class="active">
                        <?php echo $sidebar_menu2_sub4; ?>
                    </a>
                    <?php
                    }
                    else
                    {
                    ?>
                    <a href="culture.php">
                        <?php echo $sidebar_menu2_sub4; ?>
                    </a>
                    <?php
                    }
                    ?>
                </li>
            </ul>
        </li>

        <li>
            <?php
            if ($active_page == 'eset.php' || $active_page == 'greycortex.php' || $active_page == 'xopero.php' || $active_page == 'safetica.php' || $active_page == 'awanpintar.php')
            {
            ?>
            <a href="#" class="submenu-toggle active">
            <?php
            }
            else
            {
            ?>
            <a href="#" class="submenu-toggle">
            <?php
            }
            ?>
                <?php echo $sidebar_menu3; ?>
            </a>

            <ul class="submenu-wrapper">
                <li>
                    <?php
                    if ($active_page == 'eset.php') 
                    {
                    ?>
                    <a href="eset.php" class="active">
                        <?php echo $sidebar_menu3_sub1; ?>
                    </a>
                    <?php
                    }
                    else
                    {
                    ?>
                    <a href="eset.php">
                        <?php echo $sidebar_menu3_sub1; ?>
                    </a>
                    <?php
                    }
                    ?>
                </li>

                <li>
                     <?php
                    if ($active_page == 'greycortex.php') 
                    {
                    ?>
                    <a href="greycortex.php" class="active">
                        <?php echo $sidebar_menu3_sub2; ?>
                    </a>
                    <?php
                    }
                    else
                    {
                    ?>
                    <a href="greycortex.php">
                        <?php echo $sidebar_menu3_sub2; ?>
                    </a>
                    <?php
                    }
                    ?>
                </li>

                <li>
                    <?php
                    if ($active_page == 'xopero.php') 
                    {
                    ?>
                    <a href="xopero.php" class="active">
                        <?php echo $sidebar_menu3_sub3; ?>
                    </a>
                    <?php
                    }
                    else
                    {
                    ?>
                    <a href="xopero.php">
                        <?php echo $sidebar_menu3_sub3; ?>
                    </a>
                    <?php
                    }
                    ?>
                </li>

                <li>
                    <?php
                    if ($active_page == 'safetica.php') 
                    {
                    ?>
                    <a href="safetica.php" class="active">
                        <?php echo $sidebar_menu3_sub4; ?>
                    </a>
                    <?php
                    }
                    else
                    {
                    ?>
                    <a href="safetica.php">
                        <?php echo $sidebar_menu3_sub4; ?>
                    </a>
                    <?php
                    }
                    ?>
                </li>

								<li>
                    <a href="http://www.awanpintar.id" target="_blank">
											<?php echo $sidebar_menu3_sub6; ?>
                    </a>
                </li>

								<li>
                    <a href="https://www.vimanamail.id/" target="_blank">
                        VIMANAMAIL
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <?php
            if ($active_page == 'partners.php') 
            {
            ?>
            <a href="partners.php" class="active">
            <?php
            }
            else
            {
            ?>
            <a href="partners.php">
            <?php
            }
            ?>
                <?php echo $sidebar_menu4; ?>
            </a>
        </li>

        <li>
            <?php
            if ($active_page == 'reseller_registration.php') 
            {
            ?>
            <a href="reseller_registration.php" class="active">
            <?php
            }
            else
            {
            ?>
            <a href="reseller_registration.php">
            <?php
            }
            ?>
                <?php echo $sidebar_menu5; ?>
            </a>
        </li>

        <li>
            <a href="https://portal.prosperita.co.id/">
                <?php echo $sidebar_menu6; ?>
            </a>
        </li>

        <li>
            <a href="https://www.tokoeset.com/">
                <?php echo $sidebar_menu7; ?>
            </a>
        </li>

        <li>
            <a href="https://event.prosperita.co.id/">
                <?php echo $sidebar_menu8; ?>
            </a>
        </li>

        <li>
            <?php
            if ($active_page == 'contact_us.php') 
            {
            ?>
            <a href="contact_us.php" class="active">
            <?php
            }
            else
            {
            ?>
            <a href="contact_us.php">
            <?php
            }
            ?>
                <?php echo $sidebar_menu9; ?>
            </a>
        </li>

        <li>
            <?php
            if ($active_page == 'privacy.php') 
            {
            ?>
            <a href="privacy.php" class="active">
            <?php
            }
            else
            {
            ?>
            <a href="privacy.php">
            <?php
            }
            ?>
                <?php echo $sidebar_menu10; ?>
            </a>
        </li>
    </ul>
</nav>

<script type="text/javascript">
$(".sidebar-collapse").click(function(event) 
{
    $(".sidebar").toggleClass('expand');
});

$(".submenu-toggle").click(function(event) 
{
    event.preventDefault();
});
</script>