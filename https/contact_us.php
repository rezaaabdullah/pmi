<?php
	include_once 'header.php';
	
	$bg_img_name = '7.jpg';

	$en_title = 'Contact Us';
	$en_contactperson = 'Name';
	$en_compname = 'Company';
	$en_contactmail = 'Email';
	$en_subject = 'Subject';
	$en_message = 'Message';
	$en_send = 'SEND';
	$en_captcha = 'Security code';
	$en_seccode_msg = 'Invalid captcha';
	$en_success_msg = 'Success to send email';

	$id_title = 'Hubungi Kami';
	$id_contactperson = 'Nama';
	$id_compname = 'Perusahaan';
	$id_contactmail = 'Email';
	$id_subject = 'Judul';
	$id_message = 'Pesan';
	$id_send = 'KIRIM';
	$id_captcha = 'Kode keamanan';
	$id_seccode_msg = 'Kode keamanan tidak valid';
	$id_success_msg = 'Email berhasil dikirim';

	if (isset($_POST['submit'])) 
	{
		if (isset($_SESSION['tokoeset_seccode']) && isset($_POST['seccode']) && $_SESSION['tokoeset_seccode'] == md5($_POST['seccode'])) 
		{
			$headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($_POST['compname'].$_POST['contactperson'].$_POST['contactmail']));
			$jsonparam = json_encode(array('compname' => $_POST['compname'], 'contactperson' => $_POST['contactperson'], 'contactmail' => $_POST['contactmail'], 'subject' => $_POST['subject'], 'message' => $_POST['message'], 'logip' => $logip, 'loghost' => $loghost, 'logserver' => $logserver, 'logos' => $logos, 'logbrowser' => $logbrowser));
			$request = myCURL($url_apiprosperita_contactus, $headers, $jsonparam);
			$response = json_decode($request, true);
		}

		else
		{
			$seccode_status = false;
		}
	}

	include_once 'footer.php';
?>	