<?php

include templatefile($_SERVER['REQUEST_URI']);

if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') == false)
{
?>
			</div>
		</div> <!-- closing div kanan di file header.tpl -->

		<div class="bg-black-effect"></div>

		<div class="bg-image"></div>
	</div> <!-- closing div wrapper di file header.tpl -->

	<div class="footer col-xs-12">
		<div class="brand-logo">
			<center>
	            <img src="assets/img/brand-logo.png">
	        </center>
		</div>

		<div class="copyright">
			Copyright © <?php echo date('Y');?> Prosperita.
		</div>
	</div>

	<!-- Telegram -->
	<div class="telegram-content">
  <div class="telegram-icon">
    <div class="telegram-info">
      <div class="arrow-telegram">
      </div>
      <span>
        Tinggalkan Pesan
      </span>
    </div>
    <div class="backdrop">
    </div>
    <a href="https://telegram.me/ESETID" target="_blank">
      <i class="fab fa-telegram"></i>
    </a>
  </div>
</div>


	<script type="text/javascript">
	    $(document).ready(function () {
	    	$('[data-toggle="tooltip"]').tooltip({
	    		container:'body', trigger: 'hover'
	    	});
	    });

    	$(".alert").fadeTo(5000, 500).slideUp(500, function(){
		    $(".alert").slideUp(5000);
		});
    </script>

</body>
</html>

<?php
}
?>
