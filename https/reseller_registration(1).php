<?php
include_once 'header.php';

$bg_img_name = '4.jpg';

$en_title = '';//'Partner Registration';
$en_content = '';/*'<p>Kekuatan Prosperita adalah sinergi antara teknologi solusi keamanan dan strategi kemitraan yang disesuaikan dengan pasar Anda. Menyadari pentingnya jaringan mitra, kami telah merancang Program <b>Mitra Prosperita</b> untuk menjangkau kebutuhan mitra bisnis kami. Untuk itu kami mengundang Anda untuk berpartisipasi dalam mendistribusikan baik secara Online maupun Offline.</p>

	<p><b>Keuntungan menjadi reseller ESET:</b></p>
	<ol>
		<li>Produk sudah dikenal pasar</li>
		<li>Teknologi dapat diandalkan</li>
		<li>Keuntungan yang menarik</li>
		<li>Dukungan pelatihan dan tenaga ahli</li>
		<li>Dukungan materi sales dan marketing</li>
	</ol>';*/

$en_content_online_setion ='<h3>Reseller Online Registration</h3>
	<p>
	Reseller Online is for those who need promptness in obtaining the license for Customers/End-users.  The license in form of a softcopy could be obtained directly after you finish your order.
	</p>

	<p>
	You will get an account for the access to Prosperita Portal. With deposit system, you may perform any purchase within 24/7. The details of reseller online mechanism will be informed after you register. Currently, Reseller Online is available for the product of ESET Home Edition/Personal Solution and ESET Business Edition/Business Solution. Please choose according to your own capacity and register via the link below.
	</p>
	';

$en_btn_online_reg = 'Reseller Online Registration';
$en_form_online_note = 'Ini adalah halaman untuk Registrasi Reseller ONLINE. Harap isi data pada kolom ini dengan benar dan pastikan alamat email valid agar kami dapat membantu Anda dengan baik. Registrasi tidak akan diproses jika data tidak lengkap, tidak benar, atau tidak sesuai dengan peruntukan kerjasama.';
$en_type_online = 'Dengan ini saya tertarik untuk menjadi Reseller ONLINE untuk produk (pilih salah satu, atau keduanya jika Anda memiliki kemampuan menjual keduanya)';

$en_content_offline_setion ='<h3>Reseller Offline Registration</h3>

	<p>Reseller Offline is designed for you who need discussion to get the right solution before making a purchase. A minimal purchase and license in form of soft case are valid for Personal Solutions and Media Kit for Business Solution. The purchase is done by sending the Purchase Order. Please choose the registration according to your project via the link below. </p>
	';

$en_btn_offline_reg = 'Reseller Offline Registration';
$en_form_offline_note = 'Ini adalah halaman untuk Registrasi Reseller OFFLINE. Harap isi data pada kolom ini dengan benar dan pastikan alamat email valid agar kami dapat membantu Anda dengan baik. Registrasi tidak akan diproses jika data tidak lengkap, tidak benar, atau tidak sesuai dengan peruntukan kerjasama.';
$en_type_offline = 'Dengan ini saya tertarik untuk menjadi Reseller OFFLINE untuk produk (pilih salah satu, atau keduanya jika Anda memiliki kemampuan menjual keduanya)';

$en_shopname = 'Online Store Name';
$en_compname = 'Company Name';
$en_businesstype = 'Business Type';
$en_address = 'Address';
$en_country = 'Country';
$en_province= 'Province';
$en_city    = 'City';
$en_zip		= 'ZIP Code';
$en_website		= 'Website';
$en_contactperson	= 'Contact Person';
$en_email	= 'Email Address';
$en_phone	= 'Phone Number';
$en_mobile	= 'Mobile Phone Number';
$en_mobile_note = '*This number and device will be used as a token';
$en_taxno = 'Tax Number';
$en_taxcompname= 'Company Name';
$en_taxaddress = 'Address';
$en_taxprovince= 'Province';
$en_taxcity		= 'City';
$en_taxzip		= 'ZIP Code';
$en_tax_card	= 'Upload NPWP';
$en_note		= 'Note';
$en_captcha		= 'Security Code';
$en_send		= 'SEND';
$en_seccode_msg = 'Invalid captcha';
$en_success_msg = 'Data Inserted';


$id_title = 'Registrasi Reseller';
$id_content = '<p>Kekuatan Prosperita adalah sinergi antara teknologi solusi keamanan dan strategi kemitraan yang disesuaikan dengan pasar Anda. Menyadari pentingnya jaringan mitra, kami telah merancang Program Mitra Prosperita untuk menjangkau kebutuhan mitra bisnis kami. Untuk itu kami mengundang Anda untuk berpartisipasi dalam mendistribusikan baik secara Online maupun Offline.</p>

	<p><b>Keuntungan menjadi reseller Prosperita:</b></p>
	<ol>
		<li>Produk sudah dikenal pasar</li>
		<li>Teknologi dapat diandalkan</li>
		<li>Keuntungan yang menarik</li>
		<li>Dukungan pelatihan dan tenaga ahli</li>
		<li>Dukungan materi sales dan marketing</li>
	</ol>';

$id_content_online_setion ='<p><b>Bila Anda memiliki kemampuan bisnis secara online:</b></p>

	<ul>
		<li>memiliki akses internet</li>
		<li>memiliki web site untuk melakukan display/berjualan</li>
		<li>memiliki kemampuan untuk memasarkan secara Online</li>
		<li>contoh: personal, e-store, warnet, business corner</li>
	</ul>

	<p>Sistem kami akan selalu siap 24×7 untuk menyediakan lisensi bagi customer Anda, sehingga usaha Anda dapat berjalan terus sepanjang hari.</p>
	';
$id_btn_online_reg = 'Registrasi Reseller Online';
$id_form_online_note = 'Ini adalah halaman untuk Registrasi Reseller ONLINE. Harap isi data pada kolom ini dengan benar dan pastikan alamat email valid agar kami dapat membantu Anda dengan baik. Registrasi tidak akan diproses jika data tidak lengkap, tidak benar, atau tidak sesuai dengan peruntukan kerjasama.';
$id_type_online = 'Dengan ini saya tertarik untuk menjadi Reseller ONLINE untuk produk (pilih salah satu, atau keduanya jika Anda memiliki kemampuan menjual keduanya)';

$id_content_offline_setion ='<p><b>Bila Anda memiliki kemampuan bisnis secara offline:</b></p>

	<ul>
		<li>memiliki akses ke customer</li>
		<li>memiliki kemampuan memberikan solusi</li>
		<li>memiliki kemampuan untuk memasarkan</li>
		<li>memiliki lokasi berjualan yang menetap</li>
		<li>contoh: System Integrator, konsultan IT, toko komputer, toko software</li>
	</ul>

	<p>Sistem kami akan selalu siap selama jam dan hari kerja untuk menyediakan lisensi bagi customer Anda, sehingga usaha Anda dapat berjalan terus sepanjang hari.</p>
	';
$id_btn_offline_reg = 'Registrasi Reseller Offline';
$id_form_offline_note = 'Ini adalah halaman untuk Registrasi Reseller OFFLINE. Harap isi data pada kolom ini dengan benar dan pastikan alamat email valid agar kami dapat membantu Anda dengan baik. Registrasi tidak akan diproses jika data tidak lengkap, tidak benar, atau tidak sesuai dengan peruntukan kerjasama.';
$id_type_offline = 'Dengan ini saya tertarik untuk menjadi Reseller OFFLINE untuk produk (pilih salah satu, atau keduanya jika Anda memiliki kemampuan menjual keduanya)';

$id_shopname = 'Nama Toko Online';
$id_compname = 'Nama Perusahaan';
$id_businesstype = 'Tipe Usaha';
$id_address = 'Alamat';
$id_country = 'Negara';
$id_province= 'Provinsi';
$id_city    = 'Kota';
$id_zip		= 'Kode Pos';
$id_website		= 'Website';
$id_contactperson	= 'Penanggung Jawab';
$id_email	= 'Alamat Email';
$id_phone	= 'Nomor Telepon';
$id_mobile	= 'Nomor Handphone';
$id_mobile_note = '*nomor dan perangkat ini akan digunakan sebagai token';
$id_taxno = 'Nomor';
$id_taxcompname= 'Nama Perusahaan Terdaftar';
$id_taxaddress = 'Alamat';
$id_taxprovince= 'Provinsi';
$id_taxcity		= 'Kota';
$id_taxzip		= 'Kode Pos';
$id_tax_card	= 'Upload NPWP';
$id_note		= 'Catatan';
$id_captcha		= 'Kode Keamanan';
$id_send		= 'KIRIM';
$id_seccode_msg = 'Kode keamanan tidak valid';
$id_success_msg = 'Data berhasil tersimpan';

if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
	if (isset($_POST['get_province_city']))
	{
		$headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($_POST['country'].$_POST['country']));
		$jsonparam = json_encode(array('city' => $_POST['country'], 'province' => $_POST['country']));
		$request = myCURL($url_apiprosperita_get_iso, $headers, $jsonparam);
		$response = json_decode($request, true);
	}

	if (isset($_POST['get_province_city_tax']))
	{
		$headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($_POST['country'].$_POST['country']));
		$jsonparam = json_encode(array('city' => $_POST['country'], 'province' => $_POST['country']));
		$request = myCURL($url_apiprosperita_get_iso, $headers, $jsonparam);
		$response = json_decode($request, true);
	}
}
else
{
	if (isset($_POST['submit']))
	{
		if (isset($_SESSION['tokoeset_seccode']) && isset($_POST['seccode']) && $_SESSION['tokoeset_seccode'] == md5($_POST['seccode']))
		{
			$type = '';
			foreach ($_POST['type'] as $key => $value)
			{
				$type .= $value .', ';
			}

			$type = substr($type, 0, -2);

			if (isset($_GET['go']) && $_GET['go'] == 'offline-reseller')
			{
				$headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($type.$_POST['compname'].$_POST['email']));
				$jsonparam = json_encode(array('type' => $type, 'compname' => $_POST['compname'], 'businesstype' => $_POST['businesstype'], 'address' => $_POST['address'], 'city' => $_POST['city'], 'province' => $_POST['province'], 'zip' => $_POST['zip'], 'website' => $_POST['website'], 'contactperson' => $_POST['contactperson'], 'email' => $_POST['email'], 'phone' => $_POST['phone'], 'mobile' => $_POST['mobile'], 'taxno' => $_POST['taxno'], 'taxcompname' => $_POST['taxcompname'], 'taxaddress' => $_POST['taxaddress'], 'taxprovince' => $_POST['taxprovince'], 'taxzip' => $_POST['taxzip'], 'note' => $_POST['note'], 'logip' => $logip, 'loghost' => $loghost, 'logserver' => $logserver, 'logos' => $logos, 'logbrowser' => $logbrowser));
				$request = myCURL($url_apiprosperita_reg_resoffline, $headers, $jsonparam);
				$response = json_decode($request, true);
			}

			if (isset($_GET['go']) && $_GET['go'] == 'online-reseller')
			{
				if ($_FILES["tax_card"]["tmp_name"] != '')
				{
					$taxcard = base64_encode(file_get_contents($_FILES["tax_card"]["tmp_name"]));
					$taxcard_ext = strtolower(pathinfo($_FILES["tax_card"]["name"],PATHINFO_EXTENSION));
					$taxcard_size = $_FILES["tax_card"]["size"];

					if($taxcard_ext == "jpg" || $taxcard_ext == "png" || $taxcard_ext == "jpeg" || $taxcard_ext == "pdf" )
					{
						if ($taxcard_size <= 2097152)
						{
							$headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($type.$_POST['compname'].$_POST['email']));
							$jsonparam = json_encode(array('type' => $type, 'shopname' => $_POST['shopname'], 'compname' => $_POST['compname'], 'businesstype' => $_POST['businesstype'], 'address' => $_POST['address'], 'city' => $_POST['city'], 'province' => $_POST['province'], 'zip' => $_POST['zip'], 'website' => $_POST['website'], 'contactperson' => $_POST['contactperson'], 'email' => $_POST['email'], 'phone' => $_POST['phone'], 'mobile' => $_POST['mobile'], 'taxno' => $_POST['taxno'], 'taxcompname' => $_POST['taxcompname'], 'taxaddress' => $_POST['taxaddress'], 'taxprovince' => $_POST['taxprovince'], 'taxzip' => $_POST['taxzip'], 'taxfilecontent' => $taxcard, 'taxfileext' => $taxcard_ext, 'note' => $_POST['note'], 'logip' => $logip, 'loghost' => $loghost, 'logserver' => $logserver, 'logos' => $logos, 'logbrowser' => $logbrowser));
							$request = myCURL($url_apiprosperita_reg_resonline, $headers, $jsonparam);
							$response = json_decode($request, true);

						}
						else
						{
							$response['errlog'] = 'file_validation_1';
						}
					}
					else
					{
						$response['errlog'] = 'file_validation_2';
					}
				}
				else
				{
					$headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($type.$_POST['compname'].$_POST['email']));
					$jsonparam = json_encode(array('type' => $type, 'shopname' => $_POST['shopname'], 'compname' => $_POST['compname'], 'businesstype' => $_POST['businesstype'], 'address' => $_POST['address'], 'city' => $_POST['city'], 'province' => $_POST['province'], 'zip' => $_POST['zip'], 'website' => $_POST['website'], 'contactperson' => $_POST['contactperson'], 'email' => $_POST['email'], 'phone' => $_POST['phone'], 'mobile' => $_POST['mobile'], 'taxno' => $_POST['taxno'], 'taxcompname' => $_POST['taxcompname'], 'taxaddress' => $_POST['taxaddress'], 'taxprovince' => $_POST['taxprovince'], 'taxzip' => $_POST['taxzip'], 'note' => $_POST['note'], 'logip' => $logip, 'loghost' => $loghost, 'logserver' => $logserver, 'logos' => $logos, 'logbrowser' => $logbrowser));
					$request = myCURL($url_apiprosperita_reg_resonline, $headers, $jsonparam);
					$response = json_decode($request, true);
				}
			}
		}
		else
		{
			$seccode_status = false;
		}
	}
}

include_once 'footer.php';
?>
