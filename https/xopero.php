<?php
	include_once 'header.php';
	
	$bg_img_name = '5.jpg';
	$product_icon = 'xopero.png';

	$en_title = 'XOPERO';
	$en_content = '<p>Backup and Recovery Solution. Data backup becomes one of the most important protections nowadays. Xopero works very quickly when performing backup, and it is easy to operate. Xopero could use QNAP appliance as the backup storage.</p>';

	$id_title = 'XOPERO';
	$id_content = '<p>Backup and Recovery solution. Back up data saat ini menjadi salah satu proteksi yang sangat penting. Xopero bekerja sangat cepat saat melakukan back up serta mudah dalam pengoperasian. Xopero dapat menggunakan QNAP appliance sebagai backup storage.</p>';


	include_once 'footer.php';
?>	