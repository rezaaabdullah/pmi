<?php
	include_once 'header.php';
	
	$bg_img_name = '4.jpg';

	$en_title = 'Culture';
	$en_content = '<p>Although all process in our company are systemized digitally, human is still the significant driver behind it. We make sure our business is driven by acknowledging the human factor while remaining professional. The way we work as a team and how we connect with our partners and IT Leaders are based on emotional bonding we grow deliberately within our company.
		</p>';

	$id_title = 'Budaya';
	$id_content = '<p>Walaupun semua prosedur dalam perusahaan kami menggunakan sistem digital, faktor manusia di belakangnya tetap merupakan prioritas kami. Kami memastikan bisnis berjalan dengan senantiasa memberikan pengakuan kepada mereka sebagai manusia dan tetap menjaga profesionalisme. Cara kerja tim kami dan bagaimana kami menjalin relasi dengan para mitra dan para profesional IT berdasarkan pada ikatan emosional yang secara sengaja kami ciptakan sebagai budaya kinerja kami.</p>';


	include_once 'footer.php';
?>	