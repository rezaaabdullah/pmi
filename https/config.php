<?php
$dir_tpl = 	__DIR__."/../templates/";
$dir_lib = __DIR__."/../lib/";
$dir_api = "https://order.prosperita.co.id/api/devel/prosperita/";

$lang_list = array('en' => 'english', 'id' => 'indonesia');

$url_apiprosperita_get_iso = $dir_api.'apiprosperita_get_iso.php';
$url_apiprosperita_contactus = $dir_api.'apiprosperita_contactus.php';
$url_apiprosperita_reg_resoffline = $dir_api.'apiprosperita_reg_resoffline.php';
$url_apiprosperita_reg_resonline = $dir_api.'apiprosperita_reg_resonline.php';
$url_apiprosperita_remote = $dir_api.'apiprosperita_remote.php';
$url_apiprosperita_remoteapproval = $dir_api.'apiprosperita_remoteapproval.php';
$url_apiprosperita_remotecancel = $dir_api.'apiprosperita_remotecancel.php';
$url_apiprosperita_remotegetdata = $dir_api.'apiprosperita_remotegetdata.php';
$url_apiprosperita_remoteupdatedata = $dir_api.'apiprosperita_remoteupdatedata.php';

$api_err_msg =	array(
"file_validation_1"		=> "Your file too large",
"file_validation_2"		=> "Your file extension not allowed",
"prosperita_99"   => "System maintenance",
"prosperita_100"  => "Gagal Authorization web prosperita",
"prosperita_101"  => "Parameter api apiprosperita_reg_resonline tidak valid",
"prosperita_102"  => "Registrasi reseller online gagal",
"prosperita_103"  => "Parameter api apiprosperita_get_iso tidak valid",
"prosperita_104"  => "Parameter api apiprosperita_reg_resoffline tidak valid",
"prosperita_105"  => "Registrasi reseller offline gagal",
"prosperita_106"  => "Gagal kirim email data reseller online ke SS",
"prosperita_107"  => "Gagal kirim email data reseller offline ke SS",
"prosperita_108"  => "Parameter api apiprosperita_contactus tidak valid",
"prosperita_109"  => "Gagal kirim email contact us ke SS",
"prosperita_110"  => "Parameter api apiprosperita_remote tidak valid",
"prosperita_111"  => "Certificate number tidak valid",
"prosperita_112"  => "System GAGAL mengirim email ke TS",
"prosperita_113"  => "Data GAGAL disimpan di db",
"prosperita_114"  => "System GAGAL mengirim email ke technical contact person",
"prosperita_115"  => "Email dan Contact Person Teknis belum terdaftar",
"prosperita_116"  => "Parameter api apiprosperita_remoteapproval tidak valid",
"prosperita_117"  => "Data tidak valid",
"prosperita_118"  => "Data GAGAL diaktifkan di db",
"prosperita_119"  => "System GAGAL mengirim email ke customer contact person",
"prosperita_120"  => "Email dan Contact Person Account Manager belum terdaftar",
"prosperita_121"  => "Permintaan remote sudah pernah disetujui",
"prosperita_122"  => "System GAGAL mengirim email ke (TR1)",
"prosperita_123"  => "Parameter api apiprosperita_remotecancel tidak valid",
"prosperita_124"  => "Link Cancel sudah kadaluarsa",
"prosperita_125"  => "Data GAGAL dibatalkan di db",
"prosperita_126"  => "System GAGAL mengirim email ke (TS2)",
"prosperita_127"  => "System GAGAL mengirim email ke (PD2)",
"prosperita_128"  => "System GAGAL mengirim email ke (TR2)",
"prosperita_129"  => "Permintaan remote sudah pernah dibatalkan",
"prosperita_130"  => "System GAGAL mengirim email ke (TS4)",
"prosperita_131"  => "System GAGAL mengirim email ke (PD4)",
"prosperita_132"  => "System GAGAL mengirim email ke (TR4)",
"prosperita_133"  => "System GAGAL mengirim email ke (AM1)",
"prosperita_134"  => "System GAGAL mengirim email ke (TS3)",
"prosperita_135"  => "System GAGAL mengirim email ke (PD3)",
"prosperita_136"  => "System GAGAL mengirim email ke (ET3)",
"prosperita_137"  => "parameter api apiprosperita_remotegetdata tidak valid",
"prosperita_138"  => "parameter api apiprosperita_remoteupdatedata tidak valid",
"prosperita_139"  => "Data remote ID GAGAL diupdate",
"prosperita_140"  => "System GAGAL mengirim email ke (TS8)",
"prosperita_141"  => "System GAGAL mengirim email ke (ET8)",
"prosperita_142"  => "System GAGAL mengirim email ke (PD8)",
"prosperita_143"  => "Certificate Number sudah kadaluarsa"
);
?>
