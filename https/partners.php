<?php
	include_once 'header.php';
	
	$bg_img_name = '6.jpg';

	$en_title = 'Our Partners';
	$en_content = '<p>Partners/Resellers are the closest family members of Prosperita. They are the frontliners of software distribution to ensure it is appropriately delivered to your hands. More than 500 resellers are registered so far, equipped by various classifications and expertise in accordance to their trainings.</p>';

	$id_title = 'Partner Kami';
	$id_content = '';//'<p>Walaupun semua prosedur dalam perusahaan kami menggunakan sistem digital, faktor manusia di belakangnya tetap merupakan prioritas kami. Kami memastikan bisnis berjalan dengan senantiasa memberikan pengakuan kepada mereka sebagai manusia dan tetap menjaga profesionalisme. Cara kerja tim kami dan bagaimana kami menjalin relasi dengan para mitra dan para profesional IT berdasarkan pada ikatan emosional yang secara sengaja kami ciptakan sebagai budaya kinerja kami.</p>';


	include_once 'footer.php';
?>	