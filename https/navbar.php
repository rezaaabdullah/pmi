<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="socmed-item">
            <a href="https://www.linkedin.com/company/28967" target="_blank">
                <i class="fab fa-linkedin"></i>
            </a>

            <a href="https://www.instagram.com/eset.id/" target="_blank">
                <i class="fab fa-instagram"></i>
            </a>

            <a href="https://facebook.com/eset.indonesia/" target="_blank">
                <i class="fab fa-facebook-square"></i>
            </a>

            <a href="https://twitter.com/esetindonesia" target="_blank">
                <i class="fab fa-twitter-square"></i>
            </a>
        </div>

        <div class="lang-option-navbar">
            <a href="#" class="toggle-language">
                <img src="assets/img/flags/<?php echo $_COOKIE['user_lang']; ?>.jpg">
                <span><?php echo ucfirst($_COOKIE['user_lang_country']); ?></span>
            </a>

            <?php
            foreach ($lang_list as $key => $value) 
            {
            ?>
            <a href="?lang=<?php echo $key; ?>">
                <img src="assets/img/flags/<?php echo $key; ?>.jpg">
                <span><?php echo ucfirst($value); ?></span>
                <?php
                if (preg_match('/'. $key .'/', $_COOKIE['user_lang'])) 
                {
                ?>
                <i class="fas fa-circle"></i>
                <?php
                }
                ?>
            </a>
            <?php
            }
            ?>
        </div>
       
        <div class="lang-list-mobile">
            <h3 class="title">
                <?php echo $choose_lang; ?>
            </h3>

            <button class="btn-close">
                <i class="fas fa-times"></i>    
            </button>

            <?php
            foreach ($lang_list as $key => $value) 
            {
            ?>
            <a href="?lang=<?php echo $key; ?>">
                <img src="assets/img/flags/<?php echo $key; ?>.jpg">
                <span><?php echo ucfirst($value); ?></span>
                <?php
                if (preg_match('/'. $key .'/', $_COOKIE['user_lang'])) 
                {
                ?>
                <i class="fas fa-circle"></i>
                <?php
                }
                ?>
            </a>
            <?php
            }
            ?>
        </div>

        <div class='brand-icon-big'>
            <a href="index.php">
                <img src="assets/img/big_logo.png">
            </a>
        </div>

        <div class='brand-icon-small'>
            <center>
                <a href="index.php">
                    <img src="assets/img/small_logo.png">
                </a>
            </center>
        </div>

        <div class="sidebar-collapse">
            <a class="btn btn-info navbar-btn">
                <i class="fa fa-bars"></i>
            </a>
        </div>
    </div>
</nav>

<script type="text/javascript">
$(".toggle-language").click(function(event) 
{
    $(".lang-list-mobile").fadeToggle(500);
    event.preventDefault();
});

$(".lang-list-mobile .btn-close").click(function() 
{
    $(".lang-list-mobile").fadeToggle(500);
});
</script>