<?php
  session_start();

  include_once '../config.php';
  include_once $dir_lib.'hds_lib/hds_lib.php';
  include_once $dir_lib.'hds_lib/hds_setting.php';
  include_once $dir_lib.'library.php';

  $active_page = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

  if (!isset($_COOKIE['user_lang']))
  {
    include_once $dir_lib .'lang/en_lang.php';
    setcookie('user_lang', 'en', time() + (86400 * 30), '/');
    setcookie('user_lang_country', 'English', time() + (86400 * 30), '/');
    url_jump($active_page);
  }
  else
  {
    include_once $dir_lib .'lang/'. strtolower($_COOKIE['user_lang']) .'_lang.php';
  }

  if (isset($_GET['lang']))
  {
    foreach ($lang_list as $key => $value)
    {
      if (preg_match('/'. $key .'/', $_GET['lang']))
      {
        setcookie('user_lang', $_GET['lang'], time() + (86400 * 30), '/');
        setcookie('user_lang_country', $value, time() + (86400 * 30), '/');
        url_jump($active_page);
      }
    }
  }

	$bg_img_name = '7.jpg';

	$en_title = 'Remote Request Form';
	$en_certno = 'Certificate Number';
	$en_compname = 'Company Name';
  $en_contactperson = 'Name';
	$en_contactmail = 'Email';
  $en_mobile = 'Telegram ID / HP';
	$en_remoteID = 'Remote ID';
	$en_remotePass = 'Remote Password (if any)';
  $en_desc = 'Problem Description';
  $en_needsolution = 'Expected Solution / To do List:';
	$en_send = 'SEND';
	$en_captcha = 'Security code';
	$en_seccode_msg = 'Invalid captcha';
	$en_success_msg = 'Success to send email';

	$id_title = 'Form Permintaan Remote';
  $id_certno = 'Certificate Number';
	$id_compname = 'Nama Perusahaan';
  $id_contactperson = 'Nama';
	$id_contactmail = 'Email';
  $id_mobile = 'Telegram ID / HP';
	$id_remoteID = 'Remote ID';
	$id_remotePass = 'Remote Password (jika ada)';
  $id_desc = 'Deskripsi Masalah';
  $id_needsolution = 'Solusi Yang Dibutuhkan';
	$id_send = 'KIRIM';
	$id_captcha = 'Kode keamanan';
	$id_seccode_msg = 'Kode keamanan tidak valid';
	$id_success_msg = 'Email berhasil dikirim';

	if (isset($_POST['submit']))
	{
		if (isset($_SESSION['tokoeset_seccode']) && isset($_POST['seccode']) && $_SESSION['tokoeset_seccode'] == md5($_POST['seccode']))
		{
			$headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($_POST['certno'].$_POST['contactmail']));
			$jsonparam = json_encode(array('certno' => $_POST['certno'],
                   'compname' => $_POST['compname'],
                   'contactperson' => $_POST['contactperson'],
                   'contactmail' => $_POST['contactmail'],
                   'mobile' => $_POST['mobile'],
                   'remoteid' => $_POST['remoteID'],
                   'remotepass' => $_POST['remotePass'],
                   'desc' => $_POST['desc'],
                   'needsolution' => $_POST['needsolution'],
                   'logip' => $logip,
                   'loghost' => $loghost,
                   'logserver' => $logserver,
                   'logos' => $logos,
                   'logbrowser' => $logbrowser));
			$request = myCURL($url_apiprosperita_remote, $headers, $jsonparam);
			$response = json_decode($request, true);

      echo "<pre>";
      var_dump($request);
      echo "</pre>";
		}
		else
		{
			$seccode_status = false;
		}
	}

  include_once templatefile($_SERVER['REQUEST_URI']);
?>
