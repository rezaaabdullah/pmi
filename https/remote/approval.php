<?php
  session_start();

  include_once '../config.php';
  include_once $dir_lib.'hds_lib/hds_lib.php';
  include_once $dir_lib.'hds_lib/hds_setting.php';
  include_once $dir_lib.'library.php';

  if (isset($_GET["go"]))
  {
    $type = 'remoteapproval';
    $data = $_GET['go'];

    $headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($type.$data));
    $jsonparam = json_encode(array('type' => $type,
                 'data' => $data));
    echo "<pre>";
    var_dump($jsonparam);
    echo "</pre>";

    // $request = myCURL($url_apiprosperita_remoteapproval, $headers, $jsonparam);
    // $response = json_decode($request, true);
    $response["errlog"] = "0";
  }
  else
  {
    url_jump("index.php");
  }

  include_once $dir_tpl ."remote/approval.tpl";
?>
