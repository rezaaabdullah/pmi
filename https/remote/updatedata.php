<?php
  session_start();

  include_once '../config.php';
  include_once $dir_lib.'hds_lib/hds_lib.php';
  include_once $dir_lib.'hds_lib/hds_setting.php';
  include_once $dir_lib.'library.php';

  $active_page = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

  $dir_name = "remote";
  $page_name = "updatedata";

  if (isset($_GET["go"]))
  {
    if (!isset($_COOKIE['user_lang']))
    {
      include_once $dir_lib .'lang/en_lang.php';
      setcookie('user_lang', 'en', time() + (86400 * 30), '/');
      setcookie('user_lang_country', 'English', time() + (86400 * 30), '/');
      url_jump($active_page);
    }
    else
    {
      include_once $dir_lib .'lang/'. strtolower($_COOKIE['user_lang']) .'_lang.php';
    }

    if (isset($_GET['lang']))
    {
      foreach ($lang_list as $key => $value)
      {
        if (preg_match('/'. $key .'/', $_GET['lang']))
        {
          setcookie('user_lang', $_GET['lang'], time() + (86400 * 30), '/');
          setcookie('user_lang_country', $value, time() + (86400 * 30), '/');
          url_jump($active_page);
        }
      }
    }

  	if (isset($_POST['submit']))
  	{
      if (isset($_SESSION['tokoeset_seccode']) && isset($_POST['seccode']) && $_SESSION['tokoeset_seccode'] == md5($_POST['seccode']))
  		{
        $type = 'remoteupdatedata';
        $data = $_GET['go'];
        $remoteid = $_POST['remoteID'];
        $remotepass = $_POST['remotePass'];

        $headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($type.$data));
        $jsonparam = json_encode(array('type' => $type,
                     'data' => $data,
                     'remoteid' => $remoteid,
                     'remotepass' => $remotepass
                   ));
        $request = myCURL($url_apiprosperita_remoteupdatedata, $headers, $jsonparam);
        $response = json_decode($request, true);
      }
      else
  		{
  			$seccode_status = false;
  		}
  	}
    else
    {
      $type = 'remotegetdata';
      $url_param = $data = $_GET['go'];

      $headers= array("Content-type: application/json", "Accept: application/json", "Authorization: ".apiheader_auth($type.$data));
      $jsonparam = json_encode(array('type' => $type,
                   'data' => $data));
      $request = myCURL($url_apiprosperita_remotegetdata, $headers, $jsonparam);
      $response_get_data = json_decode($request, true);

      $bg_img_name = '7.jpg';

    	$en_title = 'Remote Request Form';
    	$en_compname = 'Company Name';
      $en_remoteID = 'Remote ID';
      $en_remotePass = 'Remote Password';
    	$en_send = 'SEND';
    	$en_captcha = 'Security code';

    	$id_title = 'Form Permintaan Remote';
    	$id_compname = 'Nama Perusahaan';
      $id_remoteID = 'Remote ID';
      $id_remotePass = 'Remote Password';
    	$id_send = 'KIRIM';
    	$id_captcha = 'Kode keamanan';
    }

    include_once $dir_tpl . $dir_name."/". $page_name .".tpl";
  }
  else
  {
    url_jump("index.php");
  }
?>
