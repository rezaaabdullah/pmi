<?php
	include_once 'header.php';
	
	$bg_img_name = '1.jpg';

	$en_title = 'Story of Prosperita';
	$en_content = '<p>The story of Prosperita flows since 2008. The company is officially stated  as PT Prosperita Mitra Indonesia, was the brainchild of two young professionals who interpret good life as living securely and comfortably without burden.</p>

		<p>Prosperita‘s founders who prefer to be behind the scene very much enjoy their work as Exclusive Distributor for data security softwares. Knowledge and IT Security capabilities merged with marketing expertise have been able to provide success stories by recording ESET in the 4th place of the biggest antivirus brand in Indonesia since 2014, according to IDC.</p>

		<p>The concept of living securely and comfortably without burden is communicated to all its business stakeholders, starting from Prosperita team, Customer, Resellers up to the Suppliers; through offering various solutions. The culture of profesionalism and family values are real in how Prosperita  conduct its daily activities, realizing a working environment where innovation is challenged so every team member is able to help customers and partners/resellers optimally.</p>

		<p>Partners/Resellers are the closest family members of Prosperita. They are the frontliners of software distribution to ensure it is appropriately delivered to your hands. More than 500 resellers are registered so far, equipped by various classifications and expertise in accordance to their trainings.</p>

		<p>In essence, the story of Prosperita is the story about them and YOU who believe that convenience can be achieved through living securely without burden.</p>';

	$id_title = 'Cerita Prosperita';
	$id_content = '<p>Cerita tentang Prosperita mengalir sejak tahun 2008. Perusahaan tercatat dengan nama lengkap PT Prosperita Mitra Indonesia, dibidani oleh dua orang profesional muda yang memaknai hidup nyaman adalah hidup aman tanpa beban.</p>

		<p>Pendiri Prosperita yang lebih suka berada di belakang layar sangat menikmati pekerjaannya bergelut sebagai Exclusive Distributor untuk software keamanan data. Wawasan dan kemampuan IT Security menyatu dengan keahlian marketing telah menghadirkan kisah sukses mencatat ESET dalam empat besar brand antivirus di Indonesia sejak tahun 2014 oleh IDC.</p>

		<p>Konsep hidup aman tanpa beban hadirkan kenyamanan mereka komunikasikan ke semua stakeholders bisnisnya, mulai dari tim Prosperita, Customer, Reseller sampai Suppliernya; melalui berbagai solusi yang ditawarkan. Sikap profesional dan kekeluargaan menjadi budaya nyata di kantor Prosperita, bentuk kenyamanan kerja dalam inovasi tantangan dibangun agar semua tim bisa optimal membantu Customer dan Partner/Reseller.</p>

		<p>Partner/Reseller adalah keluarga terdekat Prosperita. Mereka lah yang menjadi garis depan distribusi software tepat guna kami sampai ke tangan Anda. Lebih dari 500 Reseller terdaftar dengan berbagai klasifikasi dan tingkat keahlian sesuai pelatihan yang mereka ikuti.</p>

		<pSingkatnya, cerita tentang Prosperita adalah cerita tentang mereka dan Anda yang percaya kenyamanan bisa didapat melalui hidup aman tanpa beban.</p>';


	include_once 'footer.php';
?>	