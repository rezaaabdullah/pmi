<?php
	include_once 'header.php';
	
	$bg_img_name = '3.jpg';

	$en_title = 'Mission';
	$en_content = '<p>We embark on a mission to continuously seek out, research and test all available high quality IT security solutions from around the world and set it up to be ready for you Indonesian users. This mission involves partners as our trusted Resellers, who are equipped with adequate trainings to provide after sales services.
		</p>';

	$id_title = 'Misi';
	$id_content = '<p>Perjalanan mencapai visi dimulai dengan pencarian terus-menerus untuk memastikan keamanan data pada komputer, melakukan riset dan uji coba terhadap solusi keamanan IT berkualitas tinggi dari seluruh dunia untuk siap digunakan oleh Anda para pengguna komputer di seluruh Indonesia. Misi ini melibatkan mitra kami para Resellers, yang dibekali dengan pelatihan memadai agar dapat memberikan dukungan purna jual.</p>';


	include_once 'footer.php';
?>	