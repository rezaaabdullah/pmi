<?php
	include_once 'header.php';
	
	$bg_img_name = '5.jpg';
	$product_icon = 'flexera.png';

	$en_title = 'FLEXERA';
	$en_content = '<p>Software Vulnerability Management – Patch Management. Protection technology that uses vulnerability Intelligence to check any security gaps in your software/application. This superiority enables Flexera to notice the potential threat in more than 55,000 applications.</p>';

	$id_title = 'FLEXERA';
	$id_content = '<p>Software Vulnerability Management – Patch Management. Teknologi proteksi yang menggunakan Vulnerability Intelligence untuk melihat setiap celah keamanan yang ada pada software/aplikasi yang Anda gunakan. Kelebihan ini membuat Flexera mampu melihat potensi ancaman di lebih dari 55.000 aplikasi.</p>';


	include_once 'footer.php';
?>	