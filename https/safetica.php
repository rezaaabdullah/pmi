<?php
	include_once 'header.php';
	
	$bg_img_name = '5.jpg';
	$product_icon = 'safetica.png';

	$en_title = 'SAFETICA';
	$en_content = '<p>DLP solution. This sophisticated software made in the Czech Republic works to prevent data loss or data leak both accidentally or intentionally done by the insider or the employee of a company/institute. The user activity is monitored according to the need of the company/institute, and it can perform blocking for the next prevention.</p>';

	$id_title = 'SAFETICA';
	$id_content = '<p>DLP solution. Software canggih buatan Republik Ceko ini bekerja untuk mencegah kehilangan atau kebocoran data baik yang disengaja maupun tidak, yang dilakukan oleh orang dalam atau karyawan perusahaan/instansi. Aktivitas user dipantau sesuai kebutuhan perusahaan/instansi, serta dapat melakukan pemblokiran untuk pencegahan selanjutnya.</p>';


	include_once 'footer.php';
?>	