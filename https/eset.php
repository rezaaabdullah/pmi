<?php
	include_once 'header.php';
	
	$bg_img_name = '5.jpg';
	$product_icon = 'eset.png';

	$en_title = 'ESET';
	$en_content = '<p>
		<b>ESET Endpoint Solutions</b>
		<br>
		The right solution for computer and gadget protection of company/government institute with centralized update and management to simplify the work of IT team.
		</p>

		<p>
		<b>ESET Server Solution (File Security, Mail Security, Gateway Security)</b>
		<br>
		An advanced protection  for the server, data traffic and company emails from any security threats in order to ensure the business continuity.
		</p>

		<p>
		<b>ESET Virtualization Security</b>
		<br>
		Protection solution without agents for all virtual machines, supported by high-performance security technology without slowing the VM.
		</p>

		<p>
		<b>ESET Threat Intelligence</b>
		<br>
		In depth Information from all over the world about any threats and their sources to expand knowledge about security and take preventive measures.
		</p>

		<p>
		<b>ESET Enterprise Inspector (EDR Solution)</b>
		<br>
		Software that is used for analyzing data in the endpoint in realtime related to the security problems, of which the result could be used for prevention, detection, and response.
		</p>

		<p>
		<b>ESET Endpoint Encryption</b>
		<br>
		Encryption management software that is used to secure laptop, data, disk, and USB storage device in a company/institution so that the data could not be read by unauthorized people.
		</p>

		<p>
		<b>ESET Secure Authentication</b>
		<br>
		2FA Authentication Solution. A simple and effective way to secure company system from any unwanted access by applying two-factor authentication.
		</p>';

	$id_title = 'ESET';
	$id_content = '<p><b>ESET Endpoint Solutions</b>
		<br>
		Solusi tepat untuk proteksi komputer dan gadget perusahaan/instansi pemerintah dengan update dan manajemen terpusat untuk memudahkan kerja tim IT.</p>

		<p><b>ESET Server Solution (File Security, Mail Security, Gateway Security)</b><br>
		Solusi perlindungan tingkat lanjut terhadap server, lalulintas data dan email perusahaan dari segala ancaman keamanan, untuk memastikan kelangsungan bisnis.</p>

		<p><b>ESET Virtualization Security</b>
		Solusi perlindungan tanpa agen untuk semua mesin virtual didukung dengan teknologi keamanan berkinerja tinggi tanpa memperlambat VM.</p>

		<p><b>ESET Threat Intelligence</b><br>
		Informasi dari seluruh dunia tentang segala ancaman dan sumbernya secara mendalam untuk memperluas pengetahuan tentang keamanan dan mengambil tindakan preventif.</p>

		<p><b>ESET Enterprise Inspector (EDR Solution)</b>
		Software untuk menganalisa data pada endpoint secara realtime terkait masalah keamanan, yang hasilnya digunakan untuk pencegahan, pendeteksian dan tanggapan.</p>

		<p><b>ESET Endpoint Encryption</b><br>
		Software manajemen enkripsi yang digunakan untuk mengamankan laptop, data, disk, perangkat penyimpanan usb di perusahaan/institusi agar tidak dapat dibaca jika data tersebut jatuh ke tangan yang tidak berkepentingan.</p>

		<p><b>ESET Secure Authentication</b><br>
		2FA Authentication Solution. Cara sederhana dan efektif untuk mengamankan sistem perusahaan dari akses yang tidak diinginkan degan menerapkan otentikasi dua-faktor</p>

		<p><b>CLOUD Endpoint Security Powered by ESET</b><br>
		Solusi yang diciptakan untuk perusahaan dengan tenaga TI dan infrastruktur  terbatas, agar tetap dapat mengendalikan produk ESET yang dimiliki.</p>';


	include_once 'footer.php';
?>	