<?php
	include_once 'header.php';
	
	$bg_img_name = '2.jpg';

	$en_title = 'Vision';
	$en_content = '<p>Secure all data inside computers and networks throughout companies/institutions who understand that convenience can be created by an appropriate security software solutions while ensuring all proper safe behaviour is embedded within all Indonesian IT professionals.</p>';

	$id_title = 'Visi';
	$id_content = '<p>Mengamankan data pada komputer dan jaringan di seluruh perusahaan/institusi yang paham bahwa kenyamanan bisa dihadirkan melalui solusi keamanan dalam bentuk perangkat lunak yang tepat; dan memastikan perilaku keamanan yang benar menyatu pada semua profesional IT di Indonesia.</p>';


	include_once 'footer.php';
?>	