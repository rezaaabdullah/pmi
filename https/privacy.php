<?php
	include_once 'header.php';
	
	$bg_img_name = '8.jpg';

	$en_title = 'Privacy Policy';
	$en_content = '<p>PT Prosperita Mitra Indonesia is an Indonesian company domiciled in Bekasi, which has the full rights of the ownership and management of <a href="https://www.prosperita.co.id/">www.prosperita.co.id</a>, <a href="https://www.eset.co.id/">www.eset.co.id</a>, <a href="https://www.tokoeset.com/">www.tokoeset.com</a>, and <a href="http://www.safetica.co.id/">www.safetica.co.id</a>, and performs as an administrator of <a href="https://www.facebook.com/eset.indonesia">https://www.facebook.com/eset.indonesia</a>, Twitter @esetindonesia, and Instagram eset.id.</p>

		<p>PT Prosperita MItra Indonesia has a commitment to protect and respect your privacy. This policy becomes the basic of the private data collected from you, or the data you give to us, which will be processed by us. Please pay attention and understand carefully how we protect your private data and how we use it in accordance to Indonesian Law no. 11 from 2008 about information and electronic transaction.</p>

		<p>1. We collect the information from you</p>

		<p>1.1 We may collect and have the rights to manage your data:</p>

		<p>The private data you provide voluntarily to us when you fill out the form in the sites mentioned above, or when you subscribe, when you order certain good or service, fill out and complete survey, register online discussion, participate in a contest or a quiz, or provide email address and information collected when you navigate in one or all the sites belonging to PT Prosperita Mitra Indonesia.</p>

		<p>When you contact us, we have the right to save the correspondence records.</p>

		<p>We also may ask you to finish the survey that we use for the purpose of research and/or the internal need of the company, although you may not respond to it.</p>

		<p>The transaction details when you order and/or purchase via Website and the compliance of your order; or</p>

		<p>The details from the site visit is included, but it is not limited to data traffic, location data, weblog and other communication data, that may be used for billing or vice versa, and the data source that you access.</p>

		<p>2. IP ADDRESS AND COOKIES</p>

		<p>2.1. We collect your computer information, including the IP address (if it’s available), internet provider, operation system and the type of your browser for system administration, and report the overall information to our advertisers. This is statistical data about our users, the action and browsing pattern, and it does not refer to your individual data or data of a specific person.</p>

		<p>2.2 For the same reason, we could obtain the information about your general internet usage by using the cookies file saved in the hard drive in your computer. Cookies contains the information transferred to the hard drive in your computer. Cookies helps us improve the quality of our website and give a better and more personal service. The cookies enables us to:

		<ul>
			<li>estimate the number of respondents and the usage;</li>
			<li>save the information about your preference, which enables us to adjust our sites according to your individual interests;</li>
			<li>speed up data searching that you need; and</li>
			<li>recognize you when you get back to our sites.</li>
		</ul>
		</p>

		<p>2.3 You may refuse cookies by activating the setting in the browser that enables you to refuse cookies setting. However, when you choose this setting, you may not be able to access some parts in the sites, unless you adjust the setting of your browser to refuse cookies, our system will send cookies when you log on to our sites.</p>

		<p>2.4. It is important to know that the advertisers may use cookies, in which we have no controls, and it is not our responsibility.</p>

		<p>3.THE PLACE TO SAVE YOUR PRIVATE DATA</p>

		<p>3.1 The data that we have collected from you may be diverted and saved outside Indonesia or outside Timor Leste, or be processed by the team operating outside Indonesia and outside Timor Leste that works for us or for one of our suppliers. The team may be involved in your order compliance, transaction details management, and supporting service. By providing your private data, you agree to perform this transfer, save or manage the data. We will take all the necessary steps to make sure that your data is treated safely and in accordance to this privacy policy.</p>

		<p>3.2 All of the information that you provide for us is saved in our safe server. Each payment transaction will be encrypted by using SSL technology, in which we have given you (or in which you have chosen) the password that enables you to access the certain parts of our sites, and it is your responsibility to keep the password. We ask you to not share your password with anyone.</p>

		<p>3.3 As you know, information transfer via internet is not fully secure. Although we will do our best to protect your private data, we cannot guarantee your data safety in the sites; the outspread of your data will be at your own risk. After we receive your information, we will strengthen the procedure and the safety features to avoid the invalid access.</p>

		<p>4. THE INFORMATION USAGE</p>

		<p>4.1 We use the information about you:

		<ul>
			<li>to make sure that the content of the sites is provided in a most appropriate way and suitable to you and your device;</li>

			<li>to give information, products, or services that you ask from us or that we might think you are interested in, and you agree to be contacted via electronic media (telephone, text message, email, and so on);</li>

			<li>to enable you to participate in our interactive service features when you choose to use it; and</li>

			<li>to inform you about our service change.</li>
		</ul>
		</p>

		<p>4.2 We also may use your data to be used by the third party that we assign or choose to use your data to give you information about the product that may be interesting to you; and we or they can contact you from electronic media (telephone, text message, email, and so on);</p>

		<p>4.3 If you have become our customer or existing customer, we will only contact you via email.</p>

		<p>4.4 If you are a new customer, and we permit the third party that we choose to use your data, we will contact you via electronic media (telephone, text message, email, and so on).</p>

		<p>4.5 We do not reveal the information about the individual that could be identified by our advertisers, but we could provide them with the information about the number of our users (for example, 1000 men under 35 years old have clicked on their advertisements on a certain day). We also may use the information to help the advertisers reach a certain target. For example, women below 25 years old. We can use the private data that we collect form you to display their advertisements according to their target.</p>

		<p>5. THE EXPOSURE OF YOUR INFORMATION</p>

		<p>5.1 We can reveal your private information to the company group, which means central office, branch office, subsidiaries, holding company, business unit, point of presence, and all the parties who are lawfully related to our company.</p>

		<p>5.2 We could reveal your private information to the third party:</p>

		<p>If we are involved in sale of business purchase or asset, in this case we could reveal your private data to the prospective sellers or buyers;</p>

		<p>If our asset is acquired by the third party substantially, in which the private data of the customers belonging to us will be transferred; or</p>

		<p>If we have the responsibility to reveal or share your private data to obey legal obligation, or to protect the rights, property, or the safety of our brand, our customers, and so on. This includes information exchange with the other companies or organizations for the purpose of fraud protection and risk reduction.</p>

		<p>6. YOUR RIGHTS</p>

		<p>6.1 You have the right to ask us to not process your private data for marketing purpose. You can click unsubscribe when you receive a blast email from our system, or you can send email to ask us to inactivate your data for marketing purpose.</p>

		<p>6.2 Our website may, from time to time, contains the link to and from the network sites of our partners, advertisers, and affiliations. If you follow the link of one of the websites, you need to know that each website has its own privacy policy, and we are not responsible and do not have any obligations related to the privacy policy of the other websites. Please learn the policy before you send your private data to the sites.</p>

		<p>7. THE CHANGE IN OUR PRIVACY POLICY</p>

		<p>This policy may be changed or added anytime. Every change that we will do to our privacy policy in the future will be published in this page.</p>

		<p>YES, I have read and understood the privacy policy of PT Prosperita Mitra Indonesia and I have agreed with all of the content.</p>

		<p>Copyright © 2018 PROSPERITA</p>';

	$id_title = 'Kebijakan privasi';
	$id_content = '<p>PT Prosperita Mitra Indonesia adalah perusahaan di Indonesia yang berdomisili di Kota Bekasi dan memiliki hak penuh atas kepemilikan dan pengelolaan situs <a href="https://www.prosperita.co.id/">www.prosperita.co.id</a>, <a href="https://www.eset.co.id/">www.eset.co.id</a>, <a href="https://www.tokoeset.com/">www.tokoeset.com</a>, dan <a href="http://www.safetica.co.id/">www.safetica.co.id</a>, dan juga bertindak sebagai admin dari <a href="https://www.facebook.com/eset.indonesia">https://www.facebook.com/eset.indonesia</a>, twitter @esetindonesia, dan instagram eset.id</p>

		<p>PT Prosperita Mitra Indonesia berkomitmen untuk melindungi dan menghormati privasi Anda. Kebijakan ini menjadi dasar dimana data pribadi yang kami kumpulkan dari Anda, atau yang Anda berikan kepada kami, akan diproses oleh kami. Harap diperhatikan dan dipahami dengan seksama cara kami melindungi data pribadi Anda dan bagaimana kami akan mengunakannya sesuai UNDANG-UNDANG REPUBLIK INDONESIA NOMOR 11 TAHUN 2008 TENTANG INFORMASI DAN TRANSAKSI ELEKTRONIK</p>

		<p>1. INFORMASI dapat kami kumpulkan dari ANDA</p>

		<p>1.1 Kami dapat mengumpulkan dan berhak mengolah data dari Anda:</p>

		<p>Data pribadi yang secara suka rela Anda berikan ketika mengisi formulir di situs yang disebut di atas, atau pada saat berlangganan, memesan barang atau layanan tertentu, mengisi dan melengkapi survei, mendaftar untuk mengikuti diskusi online, berpartisipasi dalam kontes atau kuis atau memberikan alamat e-mail dan informasi yang dikumpulkan ketika Anda bernavigasi di dalam salah satu atau semua situs milik PT Prosperita Mitra Indonesia.</p>

		<p>Jika Anda menghubungi kami, kami berhak menyimpan catatan korespondensi tersebut.</p>

		<p>Kami juga akan meminta Anda untuk menyelesaikan survei yang kami gunakan untuk tujuan penelitian dan/atau kebutuhan internal perusahaan meskipun Anda tidak harus menanggapinya;</p>

		<p>Rincian transaksi Anda saat melakukan aktivitas pemesanan dan/atau pembelian melalui Website dan pemenuhan pesanan Anda; atau</p>

		<p>Rincian dari kunjungan ke situs termasuk, namun tidak terbatas pada, data lalu lintas, data lokasi, weblog dan data komunikasi lainnya, yang mungkin akan digunakan untuk penagihan atau sebaliknya dan sumber data yang Anda akses.</p>

		<p>2. ALAMAT IP DAN COOKIES</p>

		<p>2.1. Kami mengumpulkan informasi tentang komputer Anda, termasuk jika tersedia alamat IP, Internet Provider, sistem operasi dan jenis browser Anda, untuk administrasi sistem dan melaporkan informasi keseluruhan ke pengiklan kami. Ini adalah data statistik tentang pengguna kami, tindakan dan pola browsing, dan tidak mengacu pada data individu Anda atau data individu tertentu.</p>

		<p>2.2 Untuk alasan yang sama, kami dapat memperoleh informasi mengenai penggunaan internet Anda secara umum dengan menggunakan file cookies yang disimpan di hard drive komputer Anda. Cookies berisi informasi yang ditransfer ke hard drive komputer Anda. Cookies membantu kami untuk meningkatkan kualitas situs kami dan untuk memberikan layanan yang lebih baik dan lebih personal. Cookies memungkinkan kami:

			<ul>
			<li>untuk memperkirakan jumlah responden dan cara penggunaan;</li>
			<li>untuk menyimpan informasi tentang preferensi Anda, dan memungkinkan kami menyesuaikan situs kami sesuai dengan kepentingan individu Anda;</li>
			<li>untuk mempercepat pencarian data yang Anda butuhkan; dan</li>
			<li>untuk mengenali Anda ketika Anda kembali ke situs kami.</li>
			</ul>
		</p>

		<p>2.3 Anda dapat menolak menerima cookies dengan mengaktifkan pengaturan di browser yang memungkinkan Anda untuk menolak pengaturan cookies. Namun, jika Anda memilih pengaturan ini Anda mungkin tidak dapat mengakses bagian-bagian tertentu dari situs. Kecuali Anda menyesuaikan pengaturan browser Anda sehingga akan menolak cookies, sistem kami akan mengeluarkan cookies sewaktu Anda log on ke situs kami.</p>

		<p>2.4 Perlu diketahui bahwa pengiklan juga dapat menggunakan cookies, dimana kami tidak memiliki kontrol dan sepenuhnya tidak menjadi tanggung jawab kami.</p>

		<p>3. TEMPAT MENYIMPAN DATA PRIBADI ANDA</p>

		<p>3.1 Data yang kami kumpulkan dari Anda mungkin akan dialihkan, dan disimpan di luar Indonesia atau di luar Timor Leste, juga dapat diproses oleh tim yang beroperasi di luar Indonesia dan di luar Timor Leste yang bekerja untuk kami atau untuk salah satu pemasok kami. Tim tersebut mungkin terlibat dalam, antara lain, pemenuhan pesanan Anda, pengolahan rincian pembayaran dan penyediaan layanan dukungan. Dengan Mengirimkan data pribadi Anda, Anda setuju untuk transfer ini, menyimpan atau pengolahan data yang dilakukan. Kami akan mengambil semua langkah yang cukup diperlukan untuk memastikan bahwa data Anda diperlakukan dengan aman dan sesuai dengan kebijakan privasi ini.</p>

		<p>3.2 Semua informasi yang Anda berikan kepada kami disimpan pada server kami yang aman. Setiap transaksi pembayaran akan dienkripsi menggunakan teknologi SSL. Di mana kami telah memberikan Anda (atau di mana Anda telah memilih) password yang memungkinkan Anda untuk mengakses bagian-bagian tertentu dari situs kami, Anda bertanggung-jawab untuk menjaga kerahasiaan password. Kami meminta Anda tidak berbagi password dengan siapa pun.</p>

		<p>3.3 Seperti diketahui, pengiriman informasi melalui internet tidak sepenuhnya aman. Meskipun kami akan melakukan yang terbaik untuk melindungi data pribadi Anda, kami tidak dapat menjamin keamanan data Anda yang ada pada situs; masalah penyebaran data menjadi risiko Anda sendiri. Setelah kami menerima informasi Anda, kami akan memperketat prosedur dan fitur keamanan untuk mencegah akses yang tidak sah.</p>

		<p>4. PENGGUNAAN DARI INFORMASI</p>

		<p>4.1 Kami menggunakan informasi yang dimiliki mengenai Anda:</p>

		<p>untuk memastikan bahwa konten dari situs disajikan dengan cara yang paling pantas dan sesuai untuk Anda dan perangkat Anda;</p>

		<p>untuk memberikan informasi, produk atau layanan yang Anda minta dari kami atau yang kami rasa mungkin menarik bagi Anda, dan Anda setuju untuk dihubungi melalui media elektronik (telepon, sms, email, dan lain-lain);</p>

		<p>untuk memungkinkan Anda berpartisipasi dalam fitur interaktif layanan kami, ketika Anda memilih untuk melakukannya; dan</p>

		<p>untuk memberitahu Anda tentang perubahan layanan kami.</p>

		<p>4.2 Kami juga dapat menggunakan data Anda untuk dipakai oleh pihak ketiga yang kami tunjuk atau kami pilih untuk menggunakan data Anda untuk memberikan Anda informasi tentang produk yang mungkin menarik bagi Anda; dan kami atau mereka dapat menghubungi Anda melalui media elektronik (telepon, sms, email, dan lain-lain);</p>

		<p>4.3 Jika Anda sudah menjadi pelanggan atau pelanggan lama, kami hanya akan menghubungi Anda melalui email.</p>

		<p>4.4 Jika Anda adalah pelanggan baru, dan di mana kami mengijinkan pihak ketiga yang kami pilih untuk menggunakan data Anda, maka kami akan menghubungi Anda melalui media elektronik (telepon, sms, email, dan lain-lain).</p>

		<p>4.5 Kami tidak mengungkapkan informasi tentang individu yang dapat diidentifikasi oleh pengiklan kami, tapi kami dapat menyediakan mereka informasi tentang jumlah pengguna kami (misalnya, 1000 pria berusia di bawah 35 telah mengklik iklan mereka pada hari tertentu). Kami juga dapat menggunakan informasi tersebut untuk membantu pengiklan mencapai jenis sasaran tertentu, misalnya perempuan usia di bawah 25 tahun. Kami dapat menggunakan data pribadi yang kami kumpulkan dari Anda untuk menampilkan iklan mereka sesuai dengan target mereka.</p>

		<p>5. KETERBUKAAN INFORMASI ANDA</p>

		<p>5.1 Kami dapat mengungkapkan informasi pribadi Anda kepada Group perusahaan, yang berarti kantor pusat, kantor cabang, anak perusahaan, perusahaan holding, unit bisnis, point of presence, dan semua yang secara hukum memiliki keterkaitan dengan perusahaan kami.</p>

		<p>5.2 Kami dapat mengungkapkan informasi pribadi Anda kepada pihak ketiga:</p>

		<p>Jika kami terlibat dalam tindakan penjualan atau pembelian bisnis atau aset, dalam hal ini kami dapat mengungkapkan data pribadi Anda kepada calon penjual atau pembeli;</p>

		<p>Jika aset kami diakuisisi oleh pihak ketiga secara substansial, dimana data pribadi pelanggan yang dimiliki oleh pihak kami akan menjadi salah satu yang ditransfer; atau</p>

		<p>Jika kami berkewajiban untuk mengungkapkan atau berbagi data pribadi Anda untuk mematuhi kewajiban hukum, atau untuk melindungi hak, milik, atau keselamatan dari brand kami, pelanggan kami, atau lainnya. Ini termasuk pertukaran informasi dengan perusahaan dan organisasi lain untuk tujuan perlindungan penipuan dan pengurangan risiko.</p>

		<p>6. HAK ANDA</p>

		<p>6.1 Anda memiliki hak untuk meminta kami untuk tidak memroses data pribadi Anda untuk tujuan pemasaran. Anda bisa mengklik link unsubscribe jika mendapat email blast dari sistem kami atau mengirimkan email untuk meminta kami menon-aktifkan data Anda untuk keperluan pemasaran.</p>

		<p>6.2 Website kami mungkin, dari waktu ke waktu, berisi link ke dan dari situs jaringan mitra kami, pengiklan dan afiliasi. Jika Anda mengikuti link salah satu website, perlu diketahui bahwa setiap website memiliki kebijakan privasi sendiri dan bahwa kami tidak bertanggung-jawab dan memiliki kewajiban apa pun terkait dengan kebijakan privasi website lain. Harap pelajari kebijakan yang ada sebelum Anda menyerahkan data pribadi Anda ke situs tersebut.</p>

		<p>7. PERUBAHAN KEBIJAKAN PRIVASI KAMI</p>

		<p>Privasi ini dapat diubah atau ditambah sewaktu-waktu. Setiap perubahan yang kami lakukan terhadap kebijakan privasi kami di masa depan akan dipublikasikan di halaman ini.</p>

		<p>YA, dengan ini saya menyatakan sudah membaca dan memahami isi Kebijakan Privasi PT Prosperita Mitra Indonesia dan setuju dengan semua isi kebijakan yang ada.</p>

		<p>Copyright © 2018 PROSPERITA</p>';


	include_once 'footer.php';
?>	