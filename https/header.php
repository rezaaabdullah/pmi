<?php
session_start();

include_once 'config.php';
include_once $dir_lib.'hds_lib/hds_lib.php';
include_once $dir_lib.'hds_lib/hds_setting.php';
include_once $dir_lib.'library.php';

$active_page = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

if (!isset($_COOKIE['user_lang']))
{
    include_once $dir_lib .'lang/en_lang.php';
    setcookie('user_lang', 'en', time() + (86400 * 30), '/');
    setcookie('user_lang_country', 'English', time() + (86400 * 30), '/');
    url_jump($active_page);
}

else
{
    include_once $dir_lib .'lang/'. strtolower($_COOKIE['user_lang']) .'_lang.php';
}

if (isset($_GET['lang']))
{
    foreach ($lang_list as $key => $value)
    {
        if (preg_match('/'. $key .'/', $_GET['lang']))
        {
            setcookie('user_lang', $_GET['lang'], time() + (86400 * 30), '/');
            setcookie('user_lang_country', $value, time() + (86400 * 30), '/');
            url_jump($active_page);
        }
    }
}


if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') == false)
{
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="assets/img/icon.ico" />
    <title>PROSPERITA</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html" />

    <script src="assets/jquery-3.3.1.js"></script>

    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
    <script src="assets/bootstrap/js/bootstrap.js"></script>

    <link rel="stylesheet" href="assets/fontawesome/css/fontawesome-all.min.css">
    <script>
    window.FontAwesomeConfig = {
      searchPseudoElements: true
    }
    </script>

    <link rel="stylesheet" type="text/css" href="assets/css/navbar.css?v<?php echo filemtime('assets/css/navbar.css'); ?>">
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css?v<?php echo filemtime('assets/css/sidebar.css'); ?>">
    <link rel="stylesheet" type="text/css" href="assets/css/main.css?v<?php echo filemtime('assets/css/main.css'); ?>">
    <link rel="stylesheet" type="text/css" href="assets/css/footer.css?v<?php echo filemtime('assets/css/footer.css'); ?>">
    <link rel="stylesheet" type="text/css" href="assets/css/tele.css">
</head>

<body>
    <?php include_once 'navbar.php'; ?>

    <div class="wrapper">
        <?php include_once 'sidebar.php'; ?>

        <!-- Page Content Holder -->
        <div class="right">
            <div class="main-content col-xs-12">

<!-- closing div kanan ada di file footer.tpl -->
<!-- closing div wrapper ada di file footer.tpl -->

<?php
}
?>
