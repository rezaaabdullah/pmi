<?php
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
	if (isset($_POST['get_province_city']))
	{
		if (isset($response['errlog']))
		{
			if ($response['errlog'] == '0')
			{
			?>
			<div class="form-group">
				<label for="province"><?php echo ${$_COOKIE['user_lang'].'_province'}; ?>*</label>
				<select class="form-control" id="province" name="province" required>
					<?php
					for ($i=0; $i < count($response['province']); $i++)
					{
						echo '<option value="'. $response['province'][$i]['provcode'] .'">'. $response['province'][$i]['provname'] .'</option>';
					}
					?>
				</select>
			</div>

			<div class="form-group">
				<label for="city"><?php echo ${$_COOKIE['user_lang'].'_city'}; ?>*</label>
				<select class="form-control" id="city" name="city" required>
					<?php
					for ($i=0; $i < count($response['city']); $i++)
					{
						echo '<option value="'. $response['city'][$i]['cityname'] .'">'. $response['city'][$i]['cityname'] .'</option>';
					}
					?>
				</select>
			</div>
			<?php
			}
		}
	}

	if (isset($_POST['get_province_city_tax']))
	{
		if (isset($response['errlog']))
		{
			if ($response['errlog'] == '0')
			{
			?>
			<div class="form-group">
				<label for="taxprovince"><?php echo ${$_COOKIE['user_lang'].'_taxprovince'}; ?> *</label>
				<select class="form-control" id="taxprovince" name="taxprovince" required>
					<?php
					for ($i=0; $i < count($response['province']); $i++)
					{
						echo '<option value="'. $response['province'][$i]['provcode'] .'">'. $response['province'][$i]['provname'] .'</option>';
					}
					?>
				</select>
			</div>

			<div class="form-group">
				<label for="taxcity"><?php echo ${$_COOKIE['user_lang'].'_taxcity'}; ?> *</label>
				<select class="form-control" id="taxcity" name="taxcity" required>
					<?php
					for ($i=0; $i < count($response['city']); $i++)
					{
						echo '<option value="'. $response['city'][$i]['cityname'] .'">'. $response['city'][$i]['cityname'] .'</option>';
					}
					?>
				</select>
			</div>
			<?php
			}
		}
	}
}
else
{
?>
<div class="col-sm-12 col-lg-9 reseller_reg">
	<style type="text/css">
		div.bg-image
		{
		    background-image: url(assets/img/bg/<?php echo $bg_img_name; ?>);
		}

		.bg-black-effect
		{
		    opacity: 0.6;
		}

		div.main-content p
		{
			font-size: 16px;
		}
	</style>

	<h1 class="title"><?php echo ${$_COOKIE['user_lang'].'_title'}; ?></h1>

	<?php
	if (isset($_GET['go']) && ($_GET['go'] == 'online-reseller' || $_GET['go'] == 'offline-reseller'))
	{
		if (isset($seccode_status) && !$seccode_status)
		{
		?>
		<div class="alert alert-danger alert-dismissible">
	        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	        <strong>Error!</strong> <?php echo ${$_COOKIE['user_lang'].'_seccode_msg'}; ?>
	    </div>
	    <?php
		}
		else
		{
			if (isset($_POST['submit']))
			{
				if (isset($response['errlog']))
				{
					if ($response['errlog'] == '0')
					{
					?>
					<div class="alert alert-success alert-dismissible">
				        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				        <strong>Success</strong> <?php echo ${$_COOKIE['user_lang'].'_success_msg'}; ?>
				    </div>

				    <script type="text/javascript">
				    	// setTimeout("location.href = 'https://www.instagram.com/eset.id/';", 1500);
				    </script>
					<?php
					}
					else
					{
					?>
						<div class="alert alert-danger alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

							<?php
							if($response['errlog'] == "invalid type")
							{
							?>
							<strong>Error</strong> Invalid Reseller Type
							<?php
							}
							else
							{
							?>
							<strong>Error: </strong> <?php echo isset($api_err_msg[$response['errlog']]) ? $api_err_msg[$response['errlog']] : $response['errlog']; ?> 
							<?php echo isset($api_err_msg[$response['errlog']]) ? " (" .$response['errlog'] .")" : ""; ?>
							<?php
							}
							?>
					</div>
					<?php
					}
				}
				else
				{
				?>
				<div class="alert alert-danger alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>					
					<strong>Error: </strong> Invalid api response.					
				</div>
				<?php
				}
			}
		}
		?>
		<p><?php echo ($_GET['go'] == 'online-reseller') ? ${$_COOKIE['user_lang'].'_form_online_note'} : ${$_COOKIE['user_lang'].'_form_offline_note'}; ?></p>
		<form method="POST" id="form" enctype="multipart/form-data">
			<div class="form-group">
				<?php
				if ($_GET['go'] == 'online-reseller')
				{
				?>
				<label for="type"><?php echo ${$_COOKIE['user_lang'].'_type_online'}; ?> *</label><br>
				<?php
				}
				elseif ($_GET['go'] == 'offline-reseller')
				{
				?>
				<label for="type"><?php echo ${$_COOKIE['user_lang'].'_type_offline'}; ?> *</label><br>
				<?php
				}
				?>

				<input type="radio" class="form-control" id="business" name="type[]" value="1" <?php echo ($_GET['go'] == 'online-reseller') ? "disabled" : ""; ?> required>
				<label for="business">BUSINESS SOLUTIONS</label>

				<input type="radio" class="form-control" id="home" name="type[]" value="2">
				<label for="home">PERSONAL SOLUTIONS</label>
			</div>

			<?php
			if ($_GET['go'] == 'online-reseller')
			{
			?>
			<div class="form-group">
				<label for="shopname"><?php echo ${$_COOKIE['user_lang'].'_shopname'}; ?> *</label>
				<input type="shopname" class="form-control" id="shopname" name="shopname" required>
			</div>
			<?php
			}
			?>

			<div class="form-group">
				<label for="compname"><?php echo ${$_COOKIE['user_lang'].'_compname'}; ?> *</label>
				<input type="text" class="form-control" id="compname" name="compname" required>
			</div>

			<div class="form-group">
				<label for="businesstype"><?php echo ${$_COOKIE['user_lang'].'_businesstype'}; ?> *</label>
				<select class="form-control" id="businesstype" name="businesstype" required>
					<option selected>Sistem Integrator</option>
					<option>Toko Komputer</option>
					<option>Toko Aksesoris</option>
					<option>Service Komputer</option>
				</select>
			</div>

			<div class="form-group">
				<label for="address"><?php echo ${$_COOKIE['user_lang'].'_address'}; ?> *</label>
				<input type="text" class="form-control" id="address" name="address" required>
			</div>

			<div class="form-group">
				<label for="country"><?php echo ${$_COOKIE['user_lang'].'_country'}; ?> *</label>
				<select class="form-control" id="country" name="country" required>
					<option value="">- Select One -</option>
					<option value="ID">Indonesia</option>
					<option value="TL">Timor Leste</option>
				</select>
			</div>

			<div class="response-province-city">
				<div class="form-group">
					<label for="province"><?php echo ${$_COOKIE['user_lang'].'_province'}; ?> *</label>
					<select class="form-control" id="province" name="province" required disabled>
						<option>-</option>
					</select>
				</div>

				<div class="form-group">
					<label for="city"><?php echo ${$_COOKIE['user_lang'].'_city'}; ?> *</label>
					<select class="form-control" id="city" name="city" required disabled>
						<option>-</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="zip"><?php echo ${$_COOKIE['user_lang'].'_zip'}; ?> *</label>
				<input type="text" class="form-control" id="zip" name="zip" required>
			</div>

			<div class="form-group">
				<?php
				if ($_GET['go'] == 'online-reseller')
				{
				?>
					<label for="website">
						<?php echo ${$_COOKIE['user_lang'].'_website'}; ?> *
					</label>
					<input type="text" class="form-control" id="website" name="website" required>
				<?php
				}
				else
				{
				?>
					<label for="website">
						<?php echo ${$_COOKIE['user_lang'].'_website'}; ?>
					</label>
					<input type="text" class="form-control" id="website" name="website">
				<?php
				}
				?>
			</div>

			<div class="form-group">
				<label for="contactperson"><?php echo ${$_COOKIE['user_lang'].'_contactperson'}; ?> *</label>
				<input type="text" class="form-control" id="contactperson" name="contactperson" required>
			</div>

			<div class="form-group">
				<label for="email"><?php echo ${$_COOKIE['user_lang'].'_email'}; ?> *</label>
				<input type="email" class="form-control" id="email" name="email" required>
			</div>


			<div class="form-group">
				<label for="phone"><?php echo ${$_COOKIE['user_lang'].'_phone'}; ?> *</label>
				<input type="text" class="form-control" id="phone" name="phone" required>
			</div>

			<div class="form-group">
				<label for="mobile"><?php echo ${$_COOKIE['user_lang'].'_mobile'}; ?> *</label>
				<input type="text" class="form-control" id="mobile" name="mobile" required>
				<?php
				if ($_GET['go'] == 'online-reseller')
				{
				?>
				<label>
					<?php echo ${$_COOKIE['user_lang'].'_mobile_note'};?>
				</label>
				<?php
				}
				?>
			</div>

			<div class="form-group">
				<label class="col-xs-12 subtitle" style="padding: 0;">
					NPWP
				</label>
			</div>

			<div class="form-group">
				<label for="taxno"><?php echo ${$_COOKIE['user_lang'].'_taxno'}; ?> *</label>
				<input type="text" class="form-control" id="taxno" name="taxno" required>
			</div>

			<div class="form-group">
				<label for="taxcompname"><?php echo ${$_COOKIE['user_lang'].'_taxcompname'}; ?> *</label>
				<input type="text" class="form-control" id="taxcompname" name="taxcompname" required>
			</div>

			<div class="form-group">
				<label for="taxaddress"><?php echo ${$_COOKIE['user_lang'].'_taxaddress'}; ?> *</label>
				<input type="text" class="form-control" id="taxaddress" name="taxaddress" required>
			</div>

			<div class="response-province-city-tax">
				<div class="form-group">
					<label for="taxprovince"><?php echo ${$_COOKIE['user_lang'].'_taxprovince'}; ?> *</label>
					<select class="form-control" id="taxprovince" name="taxprovince" disabled required>
					<option>-</option>
					</select>
				</div>

				<div class="form-group">
					<label for="taxcity"><?php echo ${$_COOKIE['user_lang'].'_taxcity'}; ?> *</label>
					<select class="form-control" id="taxcity" name="taxcity" disabled required>
					<option>-</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label for="taxzip"><?php echo ${$_COOKIE['user_lang'].'_taxzip'}; ?> *</label>
				<input type="text" class="form-control" id="taxzip" name="taxzip" required>
			</div>

			<div class="form-group">
				<label for="tax_card"><?php echo ${$_COOKIE['user_lang'].'_tax_card'}; ?> *</label>
				<input type="file" class="form-control" id="tax_card" name="tax_card" accept=".jpg,.png,.jpeg,.pdf" style="padding: 5px 20px;" required>
				<label>*Format file: .jpg | .jpeg | .png | .pdf</label><br>
				<label>*Max Size: 500KB </label>
			</div>

			<div class="form-group">
				<label for="type"><?php echo ${$_COOKIE['user_lang'].'_comp_size'}; ?> *</label><br>

				<input type="radio" class="form-control comp-size" id="small-company" name="comp_size" value="small" required>
				<label for="small-company">Small Company</label>

				<input type="radio" class="form-control comp-size" id="medium-big-company" name="comp_size" value="medium-big" checked>
				<label for="medium-big-company">Medium / Big Company</label>
			</div>

			<div id="comp-size-response">
				<div class="form-group">
					<label for="NIB"><?php echo ${$_COOKIE['user_lang'].'_nib'}; ?> *</label>
					<input type="file" class="form-control" id="NIB" name="nib" accept=".jpg,.png,.jpeg,.pdf" style="padding: 5px 20px;" required>
					<label>*Format file: .jpg | .jpeg | .png | .pdf</label><br>
					<label>*Max Size: 500KB </label>
				</div>

				<div class="form-group">
					<label for="siup"><?php echo ${$_COOKIE['user_lang'].'_siup'}; ?> *</label>
					<input type="file" class="form-control" id="siup" name="siup" accept=".jpg,.png,.jpeg,.pdf" style="padding: 5px 20px;" required>
					<label>*Format file: .jpg | .jpeg | .png | .pdf</label><br>
					<label>*Max Size: 500KB </label>
				</div>

				<div class="form-group">
					<label for="domicile"><?php echo ${$_COOKIE['user_lang'].'_domicile'}; ?> *</label>
					<input type="file" class="form-control" id="domicile" name="domicile" accept=".jpg,.png,.jpeg,.pdf" style="padding: 5px 20px;" required>
					<label>*Format file: .jpg | .jpeg | .png | .pdf</label><br>
					<label>*Max Size: 500KB </label>
				</div>
			</div>

			<div class="form-group">
				<label for="note"><?php echo ${$_COOKIE['user_lang'].'_note'}; ?> *</label>
				<textarea class="form-control" id="note" name="note" rows="4" required></textarea>
			</div>

			<div class="form-group">
				<label><?php echo ${$_COOKIE['user_lang'].'_captcha'}; ?></label>
				<br>
				<img src="hds_seccode.php" style="width: 140px; margin-bottom: 10px;">
				<input type="text" name="seccode" class="form-control" required>
			</div>

			<div class="form-group">
				<input type="submit" class="btn btn-info" name="submit" value="<?php echo ${$_COOKIE['user_lang'].'_send'}; ?>">
			</div>
		</form>

		<script type="text/javascript">
			$('#country').change(function(event)
			{
				$.ajax({
					type: "POST",
					url: "<?php echo $active_page; ?>",
					data: {'get_province_city' : 'true', 'country' : $(this).val()},
					cache: false,
					success: function(data)
					{
						$(".response-province-city").html(data);
					},
					error: function(err)
					{
						alert('failed');
					}
				});

				$.ajax({
					type: "POST",
					url: "<?php echo $active_page; ?>",
					data: {'get_province_city_tax' : 'true', 'country' : $(this).val()},
					cache: false,
					success: function(data)
					{
						$(".response-province-city-tax").html(data);
					},
					error: function(err)
					{
						alert('failed');
					}
				});
			});

			$('#tax_card').change(function(event)
			{
				if(this.files[0].size > 500000)
				{
			       alert("Allowed max file size: 500 KB. Your file size : " + (this.files[0].size/1048576).toFixed(2) + ' MB');
			       this.value = "";
			       event.preventDefault();

			    };

			   if(!(this.files[0].type == 'image/jpg' || this.files[0].type == 'image/png' || this.files[0].type == 'image/jpeg' || this.files[0].type == 'application/pdf'))
			   {
			   		alert('Allowed file format: jpg, png, jpeg, pdf. Your file format : ' + this.files[0].type);
			   		this.value = "";
			   		event.preventDefault();
			   }
			});

			$('.comp-size').change(function(event)
			{
				let html = ``;

				if($(this).val() === 'small')
				{
					html = `
					<div class="form-group">
						<label for="NIB"><?php echo ${$_COOKIE['user_lang'].'_nib'}; ?> *</label>
						<input type="file" class="form-control" id="NIB" name="nib" accept=".jpg,.png,.jpeg,.pdf" style="padding: 5px 20px;" required>
						<label>*Format file: .jpg | .jpeg | .png | .pdf</label><br>
						<label>*Max Size: 500KB </label>
					</div>
					`;
				}
				else if($(this).val() === 'medium-big')
				{
					html = `
					<div class="form-group">
						<label for="NIB"><?php echo ${$_COOKIE['user_lang'].'_nib'}; ?> *</label>
						<input type="file" class="form-control" id="NIB" name="nib" accept=".jpg,.png,.jpeg,.pdf" style="padding: 5px 20px;" required>
						<label>*Format file: .jpg | .jpeg | .png | .pdf</label><br>
						<label>*Max Size: 500KB </label>
					</div>

					<div class="form-group">
						<label for="siup"><?php echo ${$_COOKIE['user_lang'].'_siup'}; ?> *</label>
						<input type="file" class="form-control" id="siup" name="siup" accept=".jpg,.png,.jpeg,.pdf" style="padding: 5px 20px;" required>
						<label>*Format file: .jpg | .jpeg | .png | .pdf</label><br>
						<label>*Max Size: 500KB </label>
					</div>

					<div class="form-group">
						<label for="domicile"><?php echo ${$_COOKIE['user_lang'].'_domicile'}; ?> *</label>
						<input type="file" class="form-control" id="domicile" name="domicile" accept=".jpg,.png,.jpeg,.pdf" style="padding: 5px 20px;" required>
						<label>*Format file: .jpg | .jpeg | .png | .pdf</label><br>
						<label>*Max Size: 500KB </label>
					</div>
					`;
				}

				$('#comp-size-response').html(html);
			});

			$('#NIB').change(function(event)
			{
				if(this.files[0].size > 500000)
				{
			       alert("Allowed max file size: 500 KB. Your file size : " + (this.files[0].size/1048576).toFixed(2) + ' MB');
			       this.value = "";
			       event.preventDefault();

			    };

			   if(!(this.files[0].type == 'image/jpg' || this.files[0].type == 'image/png' || this.files[0].type == 'image/jpeg' || this.files[0].type == 'application/pdf'))
			   {
			   		alert('Allowed file format: jpg, png, jpeg, pdf. Your file format : ' + this.files[0].type);
			   		this.value = "";
			   		event.preventDefault();
			   }
			});

			$('#siup').change(function(event)
			{
				if(this.files[0].size > 500000)
				{
			       alert("Allowed max file size: 500 KB. Your file size : " + (this.files[0].size/1048576).toFixed(2) + ' MB');
			       this.value = "";
			       event.preventDefault();

			    };

			   if(!(this.files[0].type == 'image/jpg' || this.files[0].type == 'image/png' || this.files[0].type == 'image/jpeg' || this.files[0].type == 'application/pdf'))
			   {
			   		alert('Allowed file format: jpg, png, jpeg, pdf. Your file format : ' + this.files[0].type);
			   		this.value = "";
			   		event.preventDefault();
			   }
			});

			$('#domicile').change(function(event)
			{
				if(this.files[0].size > 500000)
				{
			       alert("Allowed max file size: 500 KB. Your file size : " + (this.files[0].size/1048576).toFixed(2) + ' MB');
			       this.value = "";
			       event.preventDefault();

			    };

			   if(!(this.files[0].type == 'image/jpg' || this.files[0].type == 'image/png' || this.files[0].type == 'image/jpeg' || this.files[0].type == 'application/pdf'))
			   {
			   		alert('Allowed file format: jpg, png, jpeg, pdf. Your file format : ' + this.files[0].type);
			   		this.value = "";
			   		event.preventDefault();
			   }
			});

			$('#form').submit(function(event)
			{
				if ($('#business').prop('checked') == false && $('#home').prop('checked') == false)
				{
					alert('Please select product type');
					event.preventDefault();
				}

				if (!validateEmail($('#email').val()))
				{
					$('#email').css('border-color', '#e61616');
					$('#email_error').remove();
					$('#email').after('<span id="email_error" style="background: #e61616;padding: 5px 10px;margin: 5px 0; display: inline-block;">Email not valid</span>');
					event.preventDefault();
				}
				else
				{
					$('#email').css('border-color', '#fff');
					$('#email_error').remove();
				}
			});
		</script>
	<?php
	}
	else
	{
		echo ${$_COOKIE['user_lang'].'_content'};

		echo ${$_COOKIE['user_lang'].'_content_online_setion'};
	?>

	<a href="?go=online-reseller">
		<?php echo ${$_COOKIE['user_lang'].'_btn_online_reg'}; ?>
	</a>

	<?php echo ${$_COOKIE['user_lang'].'_content_offline_setion'}; ?>
	<a href="?go=offline-reseller">
		<?php echo ${$_COOKIE['user_lang'].'_btn_offline_reg'}; ?>
	</a>

	<?php
	}
	?>
</div>
<?php
}
?>
