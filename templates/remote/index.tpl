<?php
if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') == false)
{
?>
<!DOCTYPE html>
<html>
<head>
  <base href="/pmi/https/">
  <!-- <base href="/webprosperita/https/"> -->
  <link rel="shortcut icon" href="assets/img/icon.ico" />
  <title>PROSPERITA</title>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html" />

  <script src="assets/jquery-3.3.1.js"></script>

  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
  <script src="assets/bootstrap/js/bootstrap.js"></script>

  <link rel="stylesheet" href="assets/fontawesome/css/fontawesome-all.min.css">
  <script>
  window.FontAwesomeConfig = {
    searchPseudoElements: true
  }
  </script>

  <link rel="stylesheet" type="text/css" href="assets/css/navbar.css?v<?php echo filemtime('../assets/css/navbar.css'); ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css?v<?php echo filemtime('../assets/css/sidebar.css'); ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css?v<?php echo filemtime('../assets/css/main.css'); ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/footer.css?v<?php echo filemtime('../assets/css/footer.css'); ?>">
</head>

<body>
  <?php include_once '../navbar.php'; ?>

  <div class="wrapper">
    <?php include_once '../sidebar.php'; ?>

    <div class="right">
      <div class="main-content col-xs-12">
				<div class="col-sm-12 col-lg-12">
					<style type="text/css">
						div.bg-image
						{
					    background-image: url(assets/img/bg/<?php echo $bg_img_name; ?>);
						}

						.bg-black-effect
						{
					    opacity: 0.5;
						}
					</style>


					<h1 class="title"><?php echo ${$_COOKIE['user_lang'] .'_title'}; ?></h1>

					<div class="col-xs-12 col-md-7 left-content form-content" style="height: auto;">
						<?php
						if (isset($seccode_status) && !$seccode_status)
						{
						?>
						<div class="alert alert-danger alert-dismissible">
					        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					        <?php echo ${$_COOKIE['user_lang'].'_seccode_msg'}; ?>
					    </div>
					    <?php
						}
						else
						{
							if (isset($_POST['submit']))
							{                
								if (isset($response['errlog']))
								{
									if ($response['errlog'] == '0')
									{
									?>
									<div class="alert alert-success alert-dismissible">
							        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      Data sedang dicek dan menunggu persetujuan kontak teknis lisensi yang terdaftar di sistem kami.
                      Untuk mempercepat proses, dapat menghubungi kontak teknis agar melakukan approval sesuai prosedur pada email.
                      Jika dalam waktu 30 menit Anda belum menerima email, silakan hubungi Technical Support kami.
							    </div>
									<?php
									}
									else
									{
									?>
									<div class="alert alert-danger alert-dismissible">
							        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							        <?php echo $api_err_msg[$response['errlog']] ." (err: ". $response['errlog'] .")"; ?>
							    </div>
							    <?php
									}
								}
							}
						}
						?>

						<form method="POST" id="form" action="">
              <div class="form-group">
								This action require approval from License Owner.
								We're using AnyDesk to remote, please download from <a href="https://anydesk.com/en/downloads" target="_blank">https://anydesk.com/en/downloads</a> and makses sure it's installed properly to get Remote ID
							</div>

							<div class="form-group">
								<label for="certno"><?php echo ${$_COOKIE['user_lang'].'_certno'}; ?> * :</label>
								<input type="text" class="form-control" id="certno" name="certno" required>
							</div>

							<div class="form-group">
								<label for="compname"><?php echo ${$_COOKIE['user_lang'].'_compname'}; ?> * :</label>
								<input type="text" class="form-control" id="compname" name="compname" required>
							</div>

							<div class="form-group">
								<label for="contactperson"><?php echo ${$_COOKIE['user_lang'].'_contactperson'}; ?> * :</label>
								<input type="text" class="form-control" id="contactperson" name="contactperson" required>
							</div>

							<div class="form-group">
								<label for="contactmail"><?php echo ${$_COOKIE['user_lang'].'_contactmail'}; ?> * :</label>
								<input type="contactmail" class="form-control" id="contactmail" name="contactmail" required>
							</div>

              <div class="form-group">
								<label for="mobile"><?php echo ${$_COOKIE['user_lang'].'_mobile'}; ?> * :</label>
								<input type="text" class="form-control" id="mobile" name="mobile" required>
							</div>

							<div class="form-group">
								<label for="remoteID"><?php echo ${$_COOKIE['user_lang'].'_remoteID'}; ?> * :</label>
								<input type="text" class="form-control" id="remoteID" name="remoteID" required>
							</div>

							<div class="form-group">
								<label for="remotePass"><?php echo ${$_COOKIE['user_lang'].'_remotePass'}; ?>:</label>
								<input type="password" class="form-control" id="remotePass" name="remotePass">
							</div>

							<div class="form-group">
								<label for="desc"><?php echo ${$_COOKIE['user_lang'].'_desc'}; ?>:</label>
								<textarea class="form-control" id="desc" name="desc" rows="4" ></textarea>
							</div>

              <div class="form-group">
								<label for="needsolution"><?php echo ${$_COOKIE['user_lang'].'_needsolution'}; ?>:</label>
								<textarea class="form-control" id="needsolution" name="needsolution" rows="4" ></textarea>
							</div>

							<div class="form-group">
								<label for="seccode"><?php echo ${$_COOKIE['user_lang'].'_captcha'}; ?> * :</label>
								<br>
								<img src="hds_seccode.php" style="width: 140px; margin-bottom: 10px;">
								<input type="text" id="seccode" name="seccode" class="form-control" required>
							</div>

							<div class="form-group">
								<input type="submit" class="btn btn-info" name="submit" value="<?php echo ${$_COOKIE['user_lang'].'_send'}; ?>">
							</div>
						</form>

						<script type="text/javascript">
							$('#contactmail').on("keyup", function(){
								if (!validateEmail($(this).val()))
								{
									$('#contactmail').css('border-color', '#e61616');
									$('#contactmail_error').remove();
									$('#contactmail').after('<span id="contactmail_error" style="background: #e61616;padding: 5px 10px;margin: 5px 0; display: inline-block;">Email not valid</span>');
								}
								else
								{
									$('#contactmail').css('border-color', '#fff');
									$('#contactmail_error').remove();
								}
							});

							$('#form').submit(function(event)
							{
								if (!validateEmail($('#contactmail').val()))
								{
									$('#contactmail').css('border-color', '#e61616');
									$('#contactmail_error').remove();
									$('#contactmail').after('<span id="contactmail_error" style="background: #e61616;padding: 5px 10px;margin: 5px 0; display: inline-block;">Email not valid</span>');
									event.preventDefault();
								}
								else
								{
									$('#contactmail').css('border-color', '#fff');
									$('#contactmail_error').remove();
								}
							});
						</script>
					</div>

					<div class="col-xs-12 col-md-5 right-content">
						<p>
						<b>Phone:</b> <br>
						Jakarta: +62-21-5020 3727 <br>
						Semarang: +62-24-4033 2777 <br>
						Surabaya: +62-31-3360 0727 <br>
						Bali: +62-361 3350 371
						</p>

						<br>

						<p>
						<b>Email:</b> <br>
						Sales: <br>sales@prosperita.co.id<br><br>
						Technical support:  <br>support@prosperita.co.id<br><br>
						Partnership: <br>partnership@prosperita.co.id<br><br>
						</p>
					</div>
				</div>

			</div>
		</div>

		<div class="bg-black-effect"></div>

		<div class="bg-image"></div>
	</div>

	<div class="footer col-xs-12">
		<div class="brand-logo">
			<center>
				<img src="assets/img/brand-logo.png">
			</center>
		</div>

		<div class="copyright">
			Copyright © <?php echo date('Y');?> Prosperita.
		</div>
	</div>
</body>
</html>
<?php
}
?>
