<?php
if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') == false)
{
?>
<!DOCTYPE html>
<html>
<head>
  <base href="/pmi/https/">
  <!-- <base href="/webprosperita/https/"> -->
  <link rel="shortcut icon" href="assets/img/icon.ico" />
  <title>PROSPERITA</title>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html" />

  <script src="assets/jquery-3.3.1.js"></script>

  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
  <script src="assets/bootstrap/js/bootstrap.js"></script>

  <link rel="stylesheet" href="assets/fontawesome/css/fontawesome-all.min.css">
  <script>
  window.FontAwesomeConfig = {
    searchPseudoElements: true
  }
  </script>

  <link rel="stylesheet" type="text/css" href="assets/css/navbar.css?v<?php echo filemtime('../assets/css/navbar.css'); ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css?v<?php echo filemtime('../assets/css/sidebar.css'); ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css?v<?php echo filemtime('../assets/css/main.css'); ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/footer.css?v<?php echo filemtime('../assets/css/footer.css'); ?>">
</head>

<body>
  <?php
  if (isset($_POST['submit']))
  {
  ?>
  <div class="notif-page col-xs-12">
    <div class="notif-page-wrapper col-xs-12 col-sm-6">
    <?php
    if (isset($seccode_status) && !$seccode_status)
    {
    ?>
      <h1 class="notif-title">Gagal</h1>

      <i class="notif-icon failed fas fa-times"></i>

      <div class="notif-desc">
        Kode keamanan tidak valid
      </div>

      <a href="<?php echo $dir_name ."/". $page_name .".php?go=". $_GET['go']; ?>" class="btn notif-btn failed">Kembali</a>
    <?php
    }
    else
    {
      if (isset($response['errlog']))
      {
        if ($response["errlog"] == "0")
        {
        ?>
        <h1 class="notif-title">Sukses</h1>

        <i class="notif-icon fas fa-check"></i>

        <div class="notif-desc">
          Data remote telah diupdate
        </div>

        <a href="index.php" class="btn notif-btn">Selesai</a>
        <?php
        }
        else
        {
        ?>
        <h1 class="notif-title">Gagal</h1>

        <i class="notif-icon failed fas fa-times"></i>

        <div class="notif-desc">
          <?php echo $api_err_msg[$response['errlog']] ." <br> (err: ". $response['errlog'] .")"; ?>
        </div>

        <a href="index.php" class="btn notif-btn failed">Selesai</a>
        <?php
        }
      }
    }
    ?>
    </div>
  </div>
  <?php
  }
  else
  {
    include_once '../navbar.php';
  ?>

  <div class="wrapper">
    <?php include_once '../sidebar.php'; ?>

    <div class="right">
      <div class="main-content col-xs-12">
				<div class="col-sm-12 col-lg-12">
					<style type="text/css">
						div.bg-image
						{
					    background-image: url(assets/img/bg/<?php echo $bg_img_name; ?>);
						}

						.bg-black-effect
						{
					    opacity: 0.5;
						}
					</style>


					<h1 class="title"><?php echo "Update Remote ID / Password";?></h1>

					<div class="col-xs-12 col-md-7 left-content form-content" style="height: auto;">
						<?php
            if ($response_get_data["errlog"] != "0")
            {
              ?>
              <div class="alert alert-danger alert-dismissible">
					        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					        <?php echo $api_err_msg[$response_get_data["errlog"]] ." (err: ". $response_get_data["errlog"] .")"; ?>
					    </div>
					    <?php
            }
            else
            {
            ?>
  						<form method="POST" id="form" action="<?php echo $dir_name ."/". $page_name .".php?go=". $url_param; ?>">
  							<div class="form-group">
  								<label for="compname"><?php echo ${$_COOKIE['user_lang'].'_compname'}; ?>:</label>
  								<input type="text" class="form-control" id="compname" name="compname" value="<?php echo $response_get_data['arrResp']['company']; ?>"required readonly>
  							</div>

                <div class="form-group">
  								<label for="remoteID"><?php echo ${$_COOKIE['user_lang'].'_remoteID'}; ?> * :</label>
  								<input type="text" class="form-control" id="remoteID" name="remoteID" required>
  							</div>

  							<div class="form-group">
  								<label for="remotePass"><?php echo ${$_COOKIE['user_lang'].'_remotePass'}; ?> * :</label>
  								<input type="password" class="form-control" id="remotePass" name="remotePass" required>
  							</div>

                <div class="form-group">
  								<label for="seccode"><?php echo ${$_COOKIE['user_lang'].'_captcha'}; ?> * :</label>
  								<br>
  								<img src="hds_seccode.php" style="width: 140px; margin-bottom: 10px;">
  								<input type="text" id="seccode" name="seccode" class="form-control" required>
  							</div>

  							<div class="form-group">
  								<input type="submit" class="btn btn-info" name="submit" value="<?php echo ${$_COOKIE['user_lang'].'_send'}; ?>">
  							</div>
  						</form>
            <?php
            }
            ?>
					</div>

					<div class="col-xs-12 col-md-5 right-content">
						<p>
						<b>Phone:</b> <br>
						Jakarta: +62-21-5020 3727 <br>
						Semarang: +62-24-4033 2777 <br>
						Surabaya: +62-31-3360 0727 <br>
						Bali: +62-361 3350 371
						</p>

						<br>

						<p>
						<b>Email:</b> <br>
						Sales: <br>sales@prosperita.co.id<br><br>
						Technical support:  <br>support@prosperita.co.id<br><br>
						Partnership: <br>partnership@prosperita.co.id<br><br>
						</p>
					</div>
				</div>

			</div>
		</div>

		<div class="bg-black-effect"></div>

		<div class="bg-image"></div>
	</div>

	<div class="footer col-xs-12">
		<div class="brand-logo">
			<center>
				<img src="assets/img/brand-logo.png">
			</center>
		</div>

		<div class="copyright">
			Copyright © <?php echo date('Y');?> Prosperita.
		</div>
	</div>
  <?php
  }
  ?>
</body>
</html>
<?php
}
?>
