<!DOCTYPE html>
<html>
<head>
  <base href="/pmi/https/">
  <!-- <base href="/webprosperita/https/"> -->
  <link rel="shortcut icon" href="assets/img/icon.ico" />
  <title>PROSPERITA</title>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html" />

  <script src="assets/jquery-3.3.1.js"></script>

  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css">
  <script src="assets/bootstrap/js/bootstrap.js"></script>

  <link rel="stylesheet" href="assets/fontawesome/css/fontawesome-all.min.css">
  <script>
  window.FontAwesomeConfig = {
    searchPseudoElements: true
  }
  </script>

  <link rel="stylesheet" type="text/css" href="assets/css/navbar.css?v<?php echo filemtime('../assets/css/navbar.css'); ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css?v<?php echo filemtime('../assets/css/sidebar.css'); ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/main.css?v<?php echo filemtime('../assets/css/main.css'); ?>">
  <link rel="stylesheet" type="text/css" href="assets/css/footer.css?v<?php echo filemtime('../assets/css/footer.css'); ?>">
</head>

<body>
  <?php
  if (isset($_GET["go"]) && isset($response["errlog"]))
  {
  ?>
  <div class="notif-page col-xs-12">
    <div class="notif-page-wrapper col-xs-12 col-sm-6">
      <?php
      if ($response["errlog"] == "0")
      {
      ?>
      <h1 class="notif-title">Sukses</h1>

      <i class="notif-icon fas fa-check"></i>

      <div class="notif-desc">
        Permintaan remote dibatalkan
      </div>

      <a href="index.php" class="btn notif-btn">Selesai</a>
      <?php
      }
      else
      {
      ?>
      <h1 class="notif-title">Gagal</h1>

      <i class="notif-icon failed fas fa-times"></i>

      <div class="notif-desc">
        <?php echo $api_err_msg[$response['errlog']] ." <br> (err: ". $response['errlog'] .")"; ?>
      </div>

      <a href="index.php" class="btn notif-btn failed">Selesai</a>
      <?php
      }
      ?>
    </div>
  </div>
  <?php
  }
  ?>
</body>
</html>
