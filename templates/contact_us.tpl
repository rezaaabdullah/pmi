<div class="col-sm-12 col-lg-12">
	<style type="text/css">
		div.bg-image
		{
		    background-image: url(assets/img/bg/<?php echo $bg_img_name; ?>);
		}

		.bg-black-effect 
		{
		    opacity: 0.5;
		}
	</style>


	<h1 class="title"><?php echo ${$_COOKIE['user_lang'] .'_title'}; ?></h1>

	<div class="col-xs-12 col-md-7 left-content form-content" style="height: auto;">
		<?php
		if (isset($seccode_status) && !$seccode_status)
		{
		?>
		<div class="alert alert-danger alert-dismissible">
	        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	        <strong>Error!</strong> <?php echo ${$_COOKIE['user_lang'].'_seccode_msg'}; ?>
	    </div>
	    <?php
		}
		else
		{
			if (isset($_POST['submit']))
			{
				if (isset($response['errlog']))
				{
					if ($response['errlog'] == '0') 
					{
					?>
					<div class="alert alert-success alert-dismissible">
				        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				        <strong>Success</strong> <?php echo ${$_COOKIE['user_lang'].'_success_msg'}; ?>
				    </div>

				    <script type="text/javascript">
				    	setTimeout("location.href = 'https://www.instagram.com/eset.id/';", 1500);
				    </script>
					<?php
					}
	
					else
					{
					?>
					<div class="alert alert-danger alert-dismissible">
				        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				        <strong>Error (<?php echo $response['errlog']; ?>)</strong> <?php echo $api_err_msg[$response['errlog']]; ?>
				    </div>
				    <?php
					}
				}
			}
		}
		?>

		<form method="POST" id="form">
			<div class="form-group">
				<label for="contactperson"><?php echo ${$_COOKIE['user_lang'].'_contactperson'}; ?>:</label>
				<input type="text" class="form-control" id="contactperson" name="contactperson" required>
			</div>

			<div class="form-group">
				<label for="compname"><?php echo ${$_COOKIE['user_lang'].'_compname'}; ?>:</label>
				<input type="text" class="form-control" id="compname" name="compname" required>
			</div>

			<div class="form-group">
				<label for="contactmail"><?php echo ${$_COOKIE['user_lang'].'_contactmail'}; ?>:</label>
				<input type="contactmail" class="form-control" id="contactmail" name="contactmail" required>
			</div>

			<div class="form-group">
				<label for="subject"><?php echo ${$_COOKIE['user_lang'].'_subject'}; ?>:</label>
				<input type="text" class="form-control" id="subject" name="subject" required>
			</div>

			<div class="form-group">
				<label for="message"><?php echo ${$_COOKIE['user_lang'].'_message'}; ?>:</label>
				<textarea class="form-control" id="message" name="message" rows="4" required></textarea>
			</div>

			<div class="form-group">
				<label><?php echo ${$_COOKIE['user_lang'].'_captcha'}; ?>:</label>
				<br>
				<img src="hds_seccode.php" style="width: 140px; margin-bottom: 10px;">
				<input type="text" name="seccode" class="form-control" required>
			</div>

			<div class="form-group">
				<input type="submit" class="btn btn-info" name="submit" value="<?php echo ${$_COOKIE['user_lang'].'_send'}; ?>">
			</div>
		</form>

		<script type="text/javascript">
			$('#form').submit(function(event)
			{
				if (!validateEmail($('#contactmail').val()))
				{
					$('#contactmail').css('border-color', '#e61616');
					$('#contactmail_error').remove();
					$('#contactmail').after('<span id="contactmail_error" style="background: #e61616;padding: 5px 10px;margin: 5px 0; display: inline-block;">Email not valid</span>');
					event.preventDefault();
				}
				else
				{
					$('#contactmail').css('border-color', '#fff');
					$('#contactmail_error').remove();
				}
			});
		</script>
	</div>

	<div class="col-xs-12 col-md-5 right-content">
		<p>
		<b>Phone:</b> <br>
		Jakarta: +62-21-5020 3727 <br>
		Semarang: +62-24-4033 2777 <br>
		Surabaya: +62-31-3360 0727 <br>
		Bali: +62-361 3350 371
		</p>

		<br>

		<p>
		<b>Email:</b> <br>
		Sales: <br>sales@prosperita.co.id<br><br>
		Technical support:  <br>support@prosperita.co.id<br><br>
		Partnership: <br>partnership@prosperita.co.id<br><br>
		</p>
	</div>
</div>
