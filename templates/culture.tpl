<div class="col-sm-12 col-lg-8">
	<style type="text/css">
		div.bg-image
		{
		    background-image: url(assets/img/bg/<?php echo $bg_img_name; ?>);
		}

		.bg-black-effect 
		{
		    opacity: 0.5;
		}
	</style>

	<h1 class="title"><?php echo ${$_COOKIE['user_lang'].'_title'}; ?></h1>

	<?php echo ${$_COOKIE['user_lang'].'_content'}; ?>
</div>
