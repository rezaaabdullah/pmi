<div class="col-sm-12 col-lg-10 product">
	<style type="text/css">
		div.bg-image
		{
		    background-image: url(assets/img/bg/<?php echo $bg_img_name; ?>);
		}

		div.main-content p
		{
			color: #000;
			margin-top: 50%;
		}

		@media  (min-width: 1050px) and (max-width: 1305px)
		{
			div.left-content
			{
				padding-left: 0;
			}

			div.left-content img
			{
				width: 190px;
			}
		}
	</style>

	<div class="col-xs-12 col-md-4 left-content">
		<span></span>
		<img src="assets/img/products/<?php echo $product_icon; ?>">
	</div>

	<div class="col-xs-12 col-md-7 right-content">
		<?php echo ${$_COOKIE['user_lang'].'_content'}; ?>
	</div>

</div>
